<?php

use App\Http\Controllers\Admin\AdminPanelController;
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ParserController;
use App\Http\Controllers\WebsitesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/_aetos_admin', function() {
    return view('admin.auth.login');
})->name('login');

Route::post('/_aetos_admin', [AuthController::class, 'login']);
Route::post('/logout', [AuthController::class, 'logout'])->name('logout');

Route::post('sets-of-website/add-data', [WebsitesController::class, 'store'])->name('admin.create_set_for_new_website');
Route::patch('sets-of-website/edit-data/{id}', [WebsitesController::class, 'update'])->name('admin.edit_set_for_website');
Route::delete('sets-of-website/delete/{id}', [WebsitesController::class, 'destroy'])->name('admin.delete_the_set');

Route::get('/admin', [AdminPanelController::class, 'showAdminPanel'])->name('admin');
Route::get('/admin/sets', [AdminPanelController::class, 'showAllSets'])->name('admin.sets');
Route::get('/admin/sets/create', [AdminPanelController::class, 'showFormToCreateSet'])->name('admin.set.create');
Route::get('/admin/sets/edit/{id}', [AdminPanelController::class, 'showFormToEditSet'])->name('admin.set.edit');
Route::get('/admin/sets/{id}', [AdminPanelController::class, 'showSingleSet'])->name('admin.set.show');

Route::resource('/admin/categories', CategoryController::class);

Route::get('/', [ArticleController::class, 'index'])->name('home');
Route::get('/articles', [ArticleController::class, 'allArticles'])->name('articles');
Route::get('/articles/filtered-by', [ArticleController::class, 'filterArticles'])->name('articles.filtered');
Route::get('/articles/{slug}', [ArticleController::class, 'show'])->name('articles.show');
Route::get('/categories', [ArticleController::class, 'allCategories'])->name('articles.all_categories');
Route::get('/offer', [ArticleController::class, 'handleSpecialPage'])->name('offer')->defaults('pageType', 'offer');
Route::get('/partners', [ArticleController::class, 'handleSpecialPage'])->name('partners')->defaults('pageType', 'partners');
Route::get('/recommendations', [ArticleController::class, 'handleSpecialPage'])->name('recommendations')->defaults('pageType', 'recommendations');

Route::get('weather', function() {
    return view('skill_up.weather');
});
