<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Погодний додаток</title>
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body class="body-weather">
<div id="weather-app"></div>
<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
