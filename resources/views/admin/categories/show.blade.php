@extends('admin.categories.index')

@section('title', $title)

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body category-name">
                        {{ $title }}
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
                <div class="row">
                    <div class="col-xs-6">
                        <button class="btn btn-primary btn-lg custom-button-length edit-set" data-category-id="{{ $id }}">Редактировать</button>
                    </div>
                    <div class="col-xs-6 text-right">
                        <button class="btn btn-danger btn-lg custom-button-length delete-set" data-route="{{ route('categories.destroy', ['category' => $category->id]) }}">Удалить</button>
                        @php($urlForRedirect = route('admin.sets'))
                    </div>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->

        <script>
            let categoryOldName = @json($category->name);
        </script>
    </section><!-- /.content -->
@endsection
