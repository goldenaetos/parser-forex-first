<!DOCTYPE html>
<html lang="ru">

@include('../admin.head')

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    @include('../admin.header')
    @include('../admin.left_sidebar')

    <div class="content-wrapper">
        @include('../admin.breadcrumb')

        <section class="content">
            <div id="liveText">&nbsp;</div>
            <input type="text" id="categoryInput" placeholder="Введите название категории">
            <button id="saveButton">Сохранить</button>
            <div id="errorMessage" class="errorMessage"></div>

            <div id="categoryModal" class="modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body modal-text-button">
                            <p>Категория успешно добавлена</p><button id="okButton" type="button" class="btn button-modal-text btn-primary">OK</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </section>
    </div>
</div>

@include('../admin.bottom')

<script>
    document.getElementById('categoryInput').addEventListener('input', function() {
        document.getElementById('liveText').innerText = this.value;
    });

    document.getElementById('saveButton').addEventListener('click', function() {
        const categoryName = document.getElementById('categoryInput').value;

        fetch('/admin/categories', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            body: JSON.stringify({ name: categoryName})
        })
        .then(response => {
            if (response.ok) {
                return response.json();
            } else {
                const errorMessageElement = document.getElementById('errorMessage');
                errorMessageElement.style.display = "block";
                if (response.status === 422) {
                    return response.json().then(data => {
                        errorMessageElement.innerHTML = Object.values(data.errors).join('. ');
                    });
                } else {
                    errorMessageElement.innerHTML = 'Network response was not OK';
                }
            }
        })
        .then(data => {
            if(data && data.success) {
                document.getElementById('categoryModal').style.display = "block";
                document.getElementById('categoryInput').value = "";
                document.getElementById('liveText').innerHTML = "&nbsp;";
                const errorMessageElement = document.getElementById('errorMessage');
                errorMessageElement.style.display = "none";
                errorMessageElement.innerHTML = "";
            }
        });
    });

    document.getElementById('okButton').addEventListener('click', function() {
        document.getElementById('categoryModal').style.display = "none";
    });

    window.onclick = function(event) {
        const modal = document.getElementById('categoryModal');
        if (event.target === modal) {
            modal.style.display = "none";
        }
    }

</script>

</body>
</html>
