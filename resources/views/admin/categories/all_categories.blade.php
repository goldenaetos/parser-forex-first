@php
    use App\Constants\Constants;
    use Carbon\Carbon;
@endphp

@extends('admin.categories.index')

@section('title', $title)

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <table id="data-table" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Название</th>
                                <th class="table-last-updated-column">{{ Constants::LAST_UPDATED }}</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $category)
                                @if(isset($category->id))
                                    <tr class="row-of-sets">
                                        <td><a href="{{ route('categories.show', ['category' => $category->id]) }}">{{ $category->name }}</a></td>
                                        <td class="table-last-updated-column">{{ Carbon::parse($category->updated_at)->format('d.m.Y в H:i:s') }}</td>
                                        <td class="table-column-with-button">
                                            <button type="button" class="btn btn-primary btn-primary edit-set" data-category-id="{{ $category->id }}">Редактировать</button>
                                        </td>
                                        <td class="table-column-with-button">
                                            <button type="button" class="btn btn-primary btn-danger delete-set" data-route="{{ route('categories.destroy', ['category' => $category->id]) }}">Удалить</button>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Название</th>
                                <th class="table-last-updated-column">{{ Constants::LAST_UPDATED }}</th>
                                <th>&nbsp;</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@endsection
