<!-- jQuery 2.1.4 -->
<script src="{{ asset('adminpanel_content/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
<!-- Bootstrap 3.3.5 -->
<script src="{{ asset('adminpanel_content/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('adminpanel_content/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminpanel_content/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('adminpanel_content/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('adminpanel_content/dist/js/app.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('adminpanel_content/dist/js/demo.js') }}"></script>
<!-- page script -->
<script>
    $(function () {
        $("#data-table").DataTable();
    });
</script>
