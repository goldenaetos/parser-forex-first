@extends('admin.index')

@section('title', $title)

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <form id="form-for-creating-websites-set" onsubmit="submitForm(event)" data-array="{{ json_encode($arrayOfStringParameters) }}">
                <div class="box">
                    <div class="box-body">
                        <table id="data-table" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th class="table-first-column-short">№</th>
                                <th class="admin-table-second-column">Параметр</th>
                                <th class="will-be-so">Будет так</th>
                                <th class="sell-with-input">Содержимое</th>
                                <th class="table-last-column">&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($rows as $key => $value)
                                @php
                                    $valueFirstPart = substr($value, 0, 2);
                                    $valueSecondPart = substr($value, 2);
                                @endphp
                                <tr>
                                    <td class="table-first-column-short">{{ $valueFirstPart }}</td>
                                    <td class="admin-table-second-column">{{ $valueSecondPart }}</td>
                                    <td id="{{ $key }}-value" class="entering-data"></td>
                                    <td class="cell-with-input">
                                        @if($key === 'remove_image_params_patterns')
                                            <div><input id="occurrence" type="text" class="form-control" placeholder="Взаимосвязанные поля"></div>
                                            <div><input id="occurrence-additional" type="text" class="form-control"></div>
                                            <div><input id="image-pattern" type="text" class="form-control" placeholder="Взаимосвязанные поля"></div>
                                        @else
                                            <input id="{{ $key }}" type="text" class="form-control">
                                        @endif
                                    </td>
                                    <td class="table-last-column">
                                        @if(!in_array($key, $arrayOfStringParameters))
                                            <button type="button" class="btn btn-primary" onclick="addValueToArray( '{{ $key }}' )">Добавить</button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th class="table-first-column-short">№</th>
                                <th class="admin-table-second-column">Параметр</th>
                                <th class="will-be-so">Будет так</th>
                                <th class="cell-with-input">Содержимое</th>
                                <th class="table-last-column">&nbsp;</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
                <button type="submit" class="btn btn-primary btn-lg custom-button-length">Сохранить</button>
                </form>

                <script>
                    const categories = @json($categories);
                    const rows = @json($rows);
                    let dataToSend = {};
                    const arrayOfStringParameters = JSON.parse(document.getElementById('form-for-creating-websites-set').getAttribute('data-array'));

                    Object.keys(rows).forEach(rowKey => {
                        if (!arrayOfStringParameters.includes(rowKey)) {
                            dataToSend[rowKey] = [];
                        }
                    });

                    const stringInputs = arrayOfStringParameters.map(parameter => document.getElementById(parameter));

                    stringInputs.forEach(input => {
                        input.addEventListener('input', function (event) {
                            const inputValue = event.target.value;
                            const key = event.target.id;
                            const adjacentCell = event.target.closest('tr').querySelector('.entering-data');
                            adjacentCell.textContent = inputValue;
                            dataToSend[key] = inputValue;
                        });
                    });

                    document.querySelectorAll('input').forEach(input => {
                        input.addEventListener('keydown', function (event) {
                            if (event.key === 'Enter') {
                                event.preventDefault();
                                const addButton = input.closest('tr').querySelector('button');
                                if (addButton && addButton.tagName === 'BUTTON') {
                                    addButton.click();
                                }
                            }
                        });
                    });

                    function addValueToDataToSend(id, value, categoryId = null) {
                        const data = categoryId ? { url: value, category_id: categoryId } : value;

                        if (dataToSend.hasOwnProperty(id)) {
                            dataToSend[id].push(data);
                        } else {
                            dataToSend[id] = [data];
                        }
                    }

                    function createValueDiv(id, value, categorySelector = null, inputForPage) {
                        const valueDiv = document.createElement('div');
                        valueDiv.innerHTML = value + '<i class="glyphicon glyphicon-remove-circle" data-id="' + id + '"></i>';

                        if (categorySelector) {
                            inputForPage.type = 'text';
                            inputForPage.placeholder = 'URLы должны содержать';
                            inputForPage.className = 'form-control';

                            valueDiv.appendChild(categorySelector);
                            valueDiv.appendChild(inputForPage);
                        }

                        valueDiv.querySelector('.glyphicon-remove-circle').addEventListener('click', function(event) {
                            handleRemoveButtonClick(event, id, value);
                        });

                        return valueDiv;
                    }

                    function updatePageData(id, url, categoryId, foundUrlsContain) {
                        const pageIndex = dataToSend['pages'].findIndex(page => page.url === url);
                        if (pageIndex !== -1) {
                            dataToSend['pages'][pageIndex].category_id = categoryId;
                            dataToSend['pages'][pageIndex].found_urls_contain = foundUrlsContain;
                        }
                    }

                    function handleRemoveButtonClick(event, id, value) {
                        event.stopPropagation();

                        if (Array.isArray(dataToSend[id])) {
                            let index = dataToSend[id].indexOf(value);
                            if (index === -1) {
                                index = dataToSend[id].findIndex(item => item.url === value);
                            }
                            dataToSend[id].splice(index, 1);
                        } else {
                            delete dataToSend[id];
                        }

                        event.target.parentNode.remove();
                    }

                    function handleSpecialCaseForImagePatterns(id) {
                        const occurrence = document.getElementById('occurrence').value.trim();
                        const occurrenceAdditional = document.getElementById('occurrence-additional').value.trim();
                        const pattern = document.getElementById('image-pattern').value.trim();

                        if (occurrence !== '' && pattern !== '') {
                            const patternObject = { occurrence, occurrence_additional: occurrenceAdditional, pattern };
                            dataToSend[id].push(patternObject);

                            const valueContainer = document.getElementById(`${id}-value`);
                            const valueDiv = document.createElement('div');
                            const displayText = [occurrence, occurrenceAdditional || ' ', pattern].filter(Boolean).join('<br>');
                            valueDiv.innerHTML = displayText + '<i class="glyphicon glyphicon-remove-circle" data-id="' + id + '"></i>';
                            valueContainer.appendChild(valueDiv);

                            document.getElementById('occurrence').value = '';
                            document.getElementById('occurrence-additional').value = '';
                            document.getElementById('image-pattern').value = '';

                            valueDiv.querySelector('.glyphicon-remove-circle').addEventListener('click', function (event) {
                                event.stopPropagation();
                                const index = dataToSend[id].indexOf(patternObject);
                                if (index !== -1) {
                                    dataToSend[id].splice(index, 1);
                                }
                                this.parentNode.remove();
                            });
                        } else {
                            console.log("Недостаточно данных: необходимо обязательно заполнить взаимосвязанные поля");
                        }
                    }

                    function addPageValue(id, inputValue) {
                        const categorySelector = createCategorySelector();
                        const inputForPage = createInputForPage();
                        const valueDiv = createValueDiv(id, inputValue, categorySelector, inputForPage);
                        const valueContainer = document.getElementById(`${id}-value`);
                        valueContainer.appendChild(valueDiv);

                        categorySelector.addEventListener('change', function () {
                            updatePageData(id, inputValue, this.value, inputForPage.value);
                        });

                        inputForPage.addEventListener('input', function () {
                            updatePageData(id, inputValue, categorySelector.value, this.value);
                        });

                        addPageData(inputValue, categorySelector.value, inputForPage.value);
                    }

                    function addGeneralValue(id, inputValue) {
                        const valueDiv = createValueDiv(id, inputValue);
                        const valueContainer = document.getElementById(`${id}-value`);
                        valueContainer.appendChild(valueDiv);

                        addValueToDataToSend(id, inputValue);
                    }

                    function addValueToArray(id) {
                        if (id === 'remove_image_params_patterns') {
                            handleSpecialCaseForImagePatterns(id);
                        } else {
                            const input = document.getElementById(id);
                            const inputValue = input.value.trim();

                            if (inputValue !== '') {
                                if (id === 'pages') {
                                    addPageValue(id, inputValue);
                                } else {
                                    addGeneralValue(id, inputValue);
                                }
                                input.value = '';
                            }
                        }
                    }

                    function addPageData(url, categoryId, foundUrlsContain) {
                        const pageData = { url: url, category_id: categoryId, found_urls_contain: foundUrlsContain };
                        if (!dataToSend['pages']) {
                            dataToSend['pages'] = [];
                        }
                        dataToSend['pages'].push(pageData);
                    }

                    async function submitForm(event) {
                        event.preventDefault();

                        const path = "{{ route('admin.create_set_for_new_website') }}";

                        try {
                            const response = await fetch(path, {
                                method: "POST",
                                headers: {
                                    "Content-Type": "application/json",
                                    "Accept": "application/json",
                                    "X-CSRF-TOKEN": document.querySelector('meta[name="csrf-token"]').getAttribute('content')
                                },
                                body: JSON.stringify(dataToSend),
                                credentials: 'include'
                            });

                            if (!response.ok) {
                                const errorData = await response.json();
                                if (errorData.redirectUrl) {
                                    window.location.href = errorData.redirectUrl;
                                    return;
                                }
                                console.log(errorData.error);
                            } else {
                                const responseData = await response.json();
                                const table = document.querySelector('#data-table');
                                table.classList.add('hidden');
                                const button = document.querySelector('.btn-lg');
                                button.classList.add('hidden');
                                const mainArea = document.querySelector('.box-body');
                                mainArea.closest('.box').classList.add('grey-line');
                                mainArea.innerHTML = '<h2>Комплект успешно добавлен в базу данных</h2>';
                            }
                        } catch (error) {
                            console.log("Request error: ", error);
                        }
                    }

                    function addRemoveImageParamsPattern($key) {
                        const container = document.getElementById($key);
                        const newPatternDiv = document.createElement('div');
                        newPatternDiv.innerHTML = `
                            <input id="occurrence" type="text">
                            <input id="occurrence-additional" type="text">
                            <input id="image-pattern" type="text">
                        `;
                        container.appendChild(newPatternDiv);
                    }

                    function createInputForPage() {
                        return document.createElement('input');
                    }

                    function createCategorySelector() {
                        const selector = document.createElement('select');
                        selector.classList.add('form-control');

                        categories.sort((a, b) => a.name.localeCompare(b.name, undefined, { sensitivity: 'base', numeric: true }));

                        categories.forEach(category => {
                            const option = document.createElement('option');
                            option.value = category.id;
                            option.textContent = category.name;
                            selector.appendChild(option);
                        });

                        return selector;
                    }
                </script>

            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@endsection
