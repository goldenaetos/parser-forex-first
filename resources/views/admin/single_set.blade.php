@extends('admin.index')

@section('title', $title)

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <table id="data-table" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th class="table-first-column-short">№</th>
                                <th>Параметр</th>
                                <th>Содержимое</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($handledSetsArray as $key => $value)
                                @php
                                    $keyFirstPart = substr($key, 0, 2);
                                    $keySecondPart = substr($key, 2);
                                @endphp
                                <tr>
                                    <td class="table-first-column-short">{{ $keyFirstPart }}</td>
                                    <td>{{ $keySecondPart }}</td>
                                    <td>
                                        @if($keySecondPart === 'Страницы' && is_array($value))
                                            @foreach($value as $page)
                                                @if(is_array($page) && isset($page['url']))
                                                    <div>
                                                        <b>{{ $page['category']['name'] ?? 'Без категории' }}:</b>
                                                        <br>
                                                        {{ $page['url'] }}
                                                        @if(isset($page['found_urls_contain']))
                                                            <br><small><i>URLы должны содержать:</i></small>
                                                        <br>
                                                        {{ $page['found_urls_contain'] }}
                                                        @endif
                                                    </div>
                                                @endif
                                            @endforeach
                                        @elseif(is_array($value))
                                            @foreach($value as $item)
                                                @if(is_array($item))
                                                    <div class="show-single-set-remove-image-params">
                                                        <div>{{ $item['occurrence'] }}</div>
                                                        <div>{!! $item['occurrence_additional'] ?? '&nbsp;' !!}</div>
                                                        <div>{{ $item['pattern'] }}</div>
                                                    </div>
                                                @else
                                                    <div>{{ is_string($item) ? $item : json_encode($item) }}</div>
                                                @endif
                                            @endforeach
                                        @else
                                            {{ $value }}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th class="table-first-column-short">№</th>
                                <th>Параметр</th>
                                <th>Содержимое</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
                <div class="row">
                    <div class="col-xs-6">
                        <button class="btn btn-primary btn-lg custom-button-length edit-set" data-route="{{ route('admin.set.edit', ['id' => $setId]) }}">Редактировать</button>
                    </div>
                    <div class="col-xs-6 text-right">
                        <button class="btn btn-danger btn-lg custom-button-length delete-set" data-route="{{ route('admin.delete_the_set', ['id' => $setId]) }}">Удалить</button>
                        @php($urlForRedirect = route('admin.sets'))
                    </div>
                </div>

                <script>
                    document.addEventListener('DOMContentLoaded', function () {
                        const deleteSetButton = document.querySelector('.delete-set');
                        const editSetButton = document.querySelector('.edit-set');

                        editSetButton.addEventListener('click', function(event) {
                            event.preventDefault();
                            const route = this.getAttribute('data-route');
                            window.location.href = route;
                        })

                        deleteSetButton.addEventListener('click', function (event) {
                            const urlForRedirect = "{{ $urlForRedirect }}";
                            event.preventDefault();
                            if (confirm('Вы уверены, что хотите удалить этот комплект настроек?')) {
                                deleteItem(this, urlForRedirect);
                            }
                        });
                    });
                </script>

            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@endsection
