@php
    use App\Constants\Constants;
@endphp

@extends('admin.index')

@section('title', $title)

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <table id="data-table" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Название</th>
                                <th class="table-last-updated-column">{{ Constants::LAST_UPDATED }}</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($setsOfWebsites = [1, 2, 3])
                            @foreach($setsOfWebsites as $set)
                                <tr>
                                    <td>Что-то</td>
                                    <td class="table-last-updated-column">Еще что-то</td>
                                    <td class="table-column-with-button">И еще что-то</td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Название</th>
                                <th class="table-last-updated-column">{{ Constants::LAST_UPDATED }}</th>
                                <th>&nbsp;</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@endsection
