<!DOCTYPE html>
<html lang="ru">

@include('admin.head')

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    @include('admin.header')
    @include('admin.left_sidebar')

    <div class="content-wrapper">

        @include('admin.breadcrumb')

        <section class="content">
            @yield('content')

            <script src="{{ asset('adminpanel_content/js/deleteItem.js') }}"></script>
        </section>
    </div>
</div>

@include('admin.bottom')

</body>
</html>
