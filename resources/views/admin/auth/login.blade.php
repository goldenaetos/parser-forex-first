<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Login</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>

<div id="login-error" class="alert alert-danger" style="display: none;"></div>

<form id="login-form" method="POST" action="{{ route('login') }}" class="p-4">
    @csrf
    <div>
        <label for="login" class="form-label">Login:</label>
        <input type="text" id="login" name="login" class="form-control" required>
    </div>
    <div class="form-group">
        <label for="password" class="form-label">Password:</label>
        <input type="password" id="password" name="password" class="form-control" required>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Login</button>
    </div>
</form>

<script>
    $(document).ready(function () {
        $('#login-form').submit(function(e) {
            e.preventDefault();
            const formData = $(this).serialize();

            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                dataType: 'json',
                data: formData,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    if (data.redirectUrl) {
                        window.location.href = data.redirectUrl;
                    }
                },
                error: function(xhr) {
                    const error = xhr.responseJSON && xhr.responseJSON.errors ? xhr.responseJSON.errors.login[0] : 'An error occurred';
                    $('#login-error').text(error).show();
                }
            });
        });

        $('#login, #password').on('input', function() {
            $('#login-error').hide();
        });
    });
</script>

</body>
</html>
