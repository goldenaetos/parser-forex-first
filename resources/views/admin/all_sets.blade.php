@php
    use App\Constants\Constants;
    use Carbon\Carbon;
@endphp

@extends('admin.index')

@section('title', $title)

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <table id="data-table" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Название</th>
                                <th class="table-last-updated-column">{{ Constants::LAST_UPDATED }}</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($setsOfWebsites as $set)
                                <tr class="row-of-sets">
                                    <td><a href="{{ route('admin.set.show', ['id' => $set->id]) }}">{{ $set->name }}</a></td>
                                    <td class="table-last-updated-column">{{ Carbon::parse($set->updated_at)->format('d.m.Y в H:i:s') }}</td>
                                    <td class="table-column-with-button"><button type="button" class="btn btn-primary btn-danger delete-set" data-route="{{ route('admin.delete_the_set', ['id' => $set->id]) }}">Удалить</button></td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Название</th>
                                <th class="table-last-updated-column">{{ Constants::LAST_UPDATED }}</th>
                                <th>&nbsp;</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

                <script>
                    document.addEventListener('DOMContentLoaded', function () {
                        const deleteSetButtons = document.querySelectorAll('.delete-set');

                        deleteSetButtons.forEach(button => {
                            button.addEventListener('click', function (event) {
                                event.preventDefault();
                                if (confirm('Вы уверены, что хотите удалить этот комплект настроек?')) {
                                    deleteItem(this);
                                }
                            });
                        });
                    });
                </script>

            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@endsection
