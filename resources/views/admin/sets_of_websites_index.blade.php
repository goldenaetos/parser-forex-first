<html lang="ru">
<head>
    <title>
        Комплекты настроек для всех сайтов, с которых будет осуществляться парсинг
    </title>
</head>
<body>
    <h2>
        Комплекты настроек для всех сайтов, с которых будет осуществляться парсинг
    </h2>

    @foreach($setsOfWebsites as $set)
        <div>
            <p>Name: {{ $set->name }}</p>
            <p>Pages: {{ $set->pages }}</p>
            <p>String Starts: {{ $set->string_starts }}</p>
            <p>String Ends: {{ $set->string_ends }}</p>
            <p>Pattern for Detect URLs: {{ $set->pattern_for_detect_urls }}</p>
            <p>Pattern for Parsing Titles: {{ $set->pattern_for_parsing_titles }}</p>
            <p>Stop Words: {{ $set->stop_words }}</p>
            <p>Text Equals: {{ $set->text_equals }}</p>
            <p>Text Starts With: {{ $set->text_starts_with }}</p>
            <p>Text Starts With Pattern: {{ $set->text_starts_with_pattern }}</p>
            <p>Path to Image Contains: {{ $set->path_to_image_contains }}</p>
            <p>Remove Image Params Pattern: {{ $set->remove_image_params_pattern }}</p>
        </div>
    @endforeach

</body>
</html>


