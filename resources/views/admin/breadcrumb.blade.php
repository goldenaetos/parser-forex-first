<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{ $h1 }}
    </h1>
    <ol class="breadcrumb">
        @foreach($breadcrumbs as $breadcrumb)
            @if ($breadcrumb['route'] && $breadcrumb['title'] === 'Admin Panel')
                <li><a href="{{ $breadcrumb['route'] }}"><i class="fa fa-dashboard"></i>{{ $breadcrumb['title'] }}</a></li>
            @elseif($breadcrumb['route'])
                <li><a href="{{ $breadcrumb['route'] }}">{{ $breadcrumb['title'] }}</a></li>
            @else
                <li class="active">{{ $breadcrumb['title'] }}</li>
            @endif
        @endforeach
    </ol>
</section>
