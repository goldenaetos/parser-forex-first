@extends('admin.index')

@section('title', $title)

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <form id="form-for-editing-websites-set" onsubmit="submitForm(event)" data-array="{{ json_encode($arrayOfStringParameters) }}">
                    <div class="box">
                        <div class="box-body">
                            <table id="data-table" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th class="table-first-column-short">№</th>
                                    <th class="admin-table-second-column">Параметр</th>
                                    <th class="will-be-so">Будет так</th>
                                    <th class="cell-with-input">Содержимое</th>
                                    <th class="table-last-column">&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($rows as $key => $value)
                                    @php
                                        $valueFirstPart = substr($value, 0, 2);
                                        $valueSecondPart = substr($value, 2);
                                        $currentValue = $currentValues[$key] ?? '';
                                    @endphp
                                    <tr>
                                        <td class="table-first-column-short">{{ $valueFirstPart }}</td>
                                        <td>{{ $valueSecondPart }}</td>
                                        <td id="{{ $key }}-value" class="entering-data">
                                            @if($key === 'pages')
                                                <div id="pages-container">
                                                    @foreach($pages as $page)
                                                        <div class="page-with-category" data-url="{{ $page['url'] }}" data-category-id="{{ $page['category_id'] }}" data-found-urls-contain="{{ $page['found_urls_contain'] }}">
                                                            {{ $page['url'] }} &nbsp;
                                                            <select class="form-control">
                                                                @foreach($categories->sortBy('name', SORT_NATURAL|SORT_FLAG_CASE) as $category)
                                                                    <option value="{{ $category->id }}" {{ $page['category_id'] === $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                                                                @endforeach
                                                            </select>
                                                            @if(isset($page['found_urls_contain']))
                                                                <br><small><i>Обязательная часть URLов:</i></small><br>
                                                            <span>{{ $page['found_urls_contain'] }}</span>
                                                            @endif
                                                            <i class="glyphicon glyphicon-remove-circle" onclick="removeItem(this, 'pages')"></i>
                                                            <br><br>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @elseif($key === 'remove_image_params_patterns' && is_array($currentValue))
                                                @foreach($currentValue as $pattern)
                                                    <div class="remove-image-params-patterns" data-value="{{ json_encode($pattern) }}">
                                                        {{ $pattern['occurrence'] ?? '' }}<br>
                                                        {{ $pattern['occurrence_additional'] ?? ' ' }}<br>
                                                        {{ $pattern['pattern'] ?? '' }}
                                                        <i class="glyphicon glyphicon-remove-circle" onclick="removeItem(this, '{{ $key }}')"></i>
                                                    </div>
                                                @endforeach
                                            @elseif(is_array($currentValue))
                                                @foreach($currentValue as $item)
                                                    <div data-value="{{ $item }}">{{ is_string($item) ? $item : json_encode($item) }}
                                                        <i class="glyphicon glyphicon-remove-circle" onclick="removeItem(this, '{{ $key }}')"></i>
                                                    </div>
                                                @endforeach
                                            @else
                                                {{ $currentValue }}
                                            @endif
                                        </td>
                                        <td class="cell-with-input">
                                            @if($key === 'remove_image_params_patterns')
                                                <div><input id="occurrence" type="text" class="form-control" placeholder="Взаимосвязанные поля"></div>
                                                <div><input id="occurrence-additional" type="text" class="form-control"></div>
                                                <div><input id="image-pattern" type="text" class="form-control" placeholder="Взаимосвязанные поля"></div>
                                            @else
                                                <input id="{{ $key }}" type="text" class="form-control edit-input" value="{{ is_array($currentValue) ? '' : $currentValue }}">
                                            @endif
                                        </td>
                                        <td class="table-last-column">
                                            @if(!in_array($key, $arrayOfStringParameters))
                                                <button type="button" class="btn btn-primary" onclick="addValueToArray('{{ $key }}')">Добавить</button>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th class="table-first-column-short">№</th>
                                    <th>Параметр</th>
                                    <th class="will-be-so">Будет так</th>
                                    <th class="cell-with-input">Содержимое</th>
                                    <th class="table-last-column">&nbsp;</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <button type="submit" class="btn btn-primary btn-lg custom-button-length">Сохранить</button>
                </form>

                <script>
                    const rows = @json($rows);
                    const categories = @json($categories);
                    let dataToSend = {};
                    const arrayOfStringParameters = JSON.parse(document.getElementById('form-for-editing-websites-set').getAttribute('data-array'));

                    Object.keys(rows).forEach(rowKey => {
                        if (!arrayOfStringParameters.includes(rowKey)) {
                            dataToSend[rowKey] = [];
                        }
                    });

                    const stringInputs = arrayOfStringParameters.map(parameter => document.getElementById(parameter));
                    stringInputs.forEach(input => {
                        input.addEventListener('input', function (event) {
                            const inputValue = event.target.value;
                            const key = event.target.id;
                            const adjacentCell = event.target.closest('tr').querySelector('.entering-data');
                            adjacentCell.textContent = inputValue;
                            dataToSend[key] = inputValue;
                        });
                    });

                    document.querySelectorAll('input').forEach(input => {
                        input.addEventListener('keydown', function (event) {
                            if (event.key === 'Enter') {
                                event.preventDefault();
                                const addButton = input.closest('tr').querySelector('button');
                                if (addButton && addButton.tagName === 'BUTTON') {
                                    addButton.click();
                                }
                            }
                        });
                    });

                    function addPage(id) {
                        const pagesContainer = document.getElementById('pages-container');
                        const pageUrlInput = document.getElementById(id).value.trim();

                        if (pageUrlInput) {
                            const newPageDiv = createDomElement('page', pageUrlInput, categories);
                            pagesContainer.appendChild(newPageDiv);

                            const newCategorySelect = newPageDiv.querySelector('select');
                            const categoryId = newCategorySelect[0].value;
                            const newInput = newPageDiv.querySelector('input');

                            dataToSend['pages'].push({ url: pageUrlInput, category_id: categoryId, found_urls_contain: newInput.value });

                            newCategorySelect.addEventListener('change', function () {
                                updatePageData(this);
                            });

                            newInput.addEventListener('input', function () {
                                updatePageData(this);
                            })

                            document.getElementById(id).value = '';
                        }
                    }

                    function addImageParamsPattern(id) {
                        const occurrence = document.getElementById('occurrence').value.trim();
                        const occurrenceAdditional = document.getElementById('occurrence-additional').value.trim();
                        const pattern = document.getElementById('image-pattern').value.trim();

                        if (occurrence && pattern) {
                            const patternObject = { occurrence, occurrence_additional: occurrenceAdditional, pattern };
                            dataToSend[id].push(patternObject);

                            const valueContainer = document.getElementById(`${id}-value`);
                            const displayText = [occurrence, occurrenceAdditional || ' ', pattern].filter(Boolean).join('<br>');
                            const valueDiv = createDomElement('pattern', displayText, null, id);
                            valueContainer.appendChild(valueDiv);

                            clearInputFields(['occurrence', 'occurrence-additional', 'image-pattern']);
                        } else {
                            console.log("Недостаточно данных: необходимо обязательно заполнить взаимосвязанные поля");
                        }
                    }

                    function addGeneralValue(id) {
                        const input = document.getElementById(id);
                        const inputValue = input.value.trim();

                        if (inputValue) {
                            if (!dataToSend[id]) {
                                dataToSend[id] = [];
                            }
                            dataToSend[id].push(inputValue);

                            const valueContainer = document.getElementById(`${id}-value`);
                            const valueDiv = createDomElement('general', inputValue, null, id);
                            valueContainer.appendChild(valueDiv);

                            input.value = '';
                        }
                    }

                    function clearInputFields(ids) {
                        ids.forEach(id => {
                            document.getElementById(id).value = '';
                        });
                    }

                    function createDomElement(type, content, categories, id) {
                        let element = document.createElement('div');

                        switch (type) {
                            case 'page':
                                element.className = 'page-with-category';
                                element.setAttribute('data-url', content);
                                categories.sort((a, b) => a.name.localeCompare(b.name, undefined, { sensitivity: 'base', numeric: true }));
                                let categoryOptions = categories.map(category =>
                                    `<option value="${category.id}">${category.name}</option>`
                                ).join('');
                                element.innerHTML = `
                                    ${content}<br>
                                    Обязательная часть URLов:<br>
                                    <input><br>
                                    <select class="form-control">${categoryOptions}</select>
                                    <i class="glyphicon glyphicon-remove-circle" onclick="removeItem(this, 'pages')"></i>
                                `;
                                break;

                            case 'pattern':
                                element.className = 'remove-image-params-patterns';
                                element.setAttribute('data-value', JSON.stringify(content));
                                element.innerHTML = `
                                    ${content}
                                    <i class="glyphicon glyphicon-remove-circle" onclick="removeItem(this, '${id}')"></i>
                                `;
                                break;

                            case 'general':
                                element.className = 'general-value';
                                element.setAttribute('data-value', content);
                                element.innerHTML = `
                                    ${content}
                                    <i class="glyphicon glyphicon-remove-circle" onclick="removeItem(this, '${id}')"></i>
                                `;
                                break;
                        }

                        return element;
                    }

                    function addValueToArray(id) {
                        switch (id) {
                            case 'pages':
                                addPage(id);
                                break;
                            case 'remove_image_params_patterns':
                                addImageParamsPattern(id);
                                break;
                            default:
                                addGeneralValue(id);
                        }
                    }

                    async function submitForm(event) {
                        event.preventDefault();

                        const path = "{{ route('admin.edit_set_for_website', ['id' => $setId]) }}";

                        try {
                            const response = await fetch(path, {
                                method: "PATCH",
                                headers: {
                                    "Content-Type": "application/json",
                                    'Accept': 'application/json',
                                    "X-CSRF-TOKEN": document.querySelector('meta[name="csrf-token"]').getAttribute('content')
                                },
                                body: JSON.stringify(dataToSend)
                            });

                            if (!response.ok) {
                                const errorData = await response.json();
                                console.log(errorData.error);
                            } else {
                                const responseData = await response.json();
                                const table = document.querySelector('#data-table');
                                table.classList.add('hidden');
                                const button = document.querySelector('.btn-lg');
                                button.classList.add('hidden');
                                const mainArea = document.querySelector('.box-body');
                                mainArea.closest('.box').classList.add('grey-line');
                                mainArea.innerHTML = '<h2>Комплект успешно обновлен</h2>';
                            }
                        } catch (error) {
                            console.log("Request error: ", error);
                        }
                    }

                    function removeItem(element, key) {
                        if (key === 'pages') {
                            const pageDiv = element.closest('.page-with-category');
                            const url = pageDiv.getAttribute('data-url');

                            dataToSend[key] = dataToSend[key].filter(item => item.url !== url);

                            pageDiv.remove();
                        } else {
                            let valueToRemove = element.parentNode.getAttribute('data-value');
                            try {
                                valueToRemove = JSON.parse(valueToRemove);
                            } catch (e) {
                                // If the value is not a JSON string, leave it as is
                            }

                            if (typeof valueToRemove === 'object' && valueToRemove !== null) {
                                dataToSend[key] = dataToSend[key].filter(item => {
                                    return !(item.occurrence === valueToRemove.occurrence &&
                                        item.occurrence_additional === valueToRemove.occurrence_additional &&
                                        item.pattern === valueToRemove.pattern);
                                });
                            } else {
                                const index = dataToSend[key].indexOf(String(valueToRemove));
                                if (index !== -1) {
                                    dataToSend[key].splice(index, 1);
                                }
                            }

                            element.parentNode.remove();
                        }
                    }

                    function updatePageData(selectElement) {
                        const pageDiv = selectElement.closest('.page-with-category');
                        const url = pageDiv.getAttribute('data-url');
                        let categoryId = null;

                        if (selectElement.tagName === 'SELECT') {
                            categoryId = selectElement.value;
                        }

                        const foundUrlsContain = pageDiv.querySelector('span') ? pageDiv.querySelector('span').innerText : pageDiv.querySelector('input') ? pageDiv.querySelector('input').value : null;

                        const pageIndex = dataToSend['pages'].findIndex(item => item.url === url);
                        if (pageIndex >= 0) {
                            if (categoryId) {
                                dataToSend['pages'][pageIndex].category_id = categoryId;
                            }
                            dataToSend['pages'][pageIndex].found_urls_contain = foundUrlsContain;
                        }
                    }

                    function initializeDataToSend () {
                        Object.keys(rows).forEach(rowKey => {
                            if (rowKey === 'remove_image_params_patterns') {
                                const existingValues = [...document.querySelectorAll(`#${rowKey}-value div`)].map(div => {
                                    try {
                                        return JSON.parse(div.getAttribute('data-value'));
                                    } catch (e) {
                                        return div.getAttribute('data-value');
                                    }
                                });
                                dataToSend[rowKey] = existingValues.length > 0 ? existingValues : [];
                            } else if (rowKey === 'pages') {
                                const pageElements = document.querySelectorAll('#pages-container div');
                                const pageData = Array.from(pageElements).map(div => {
                                    const url = div.getAttribute('data-url');
                                    const categoryId = div.getAttribute('data-category-id');
                                    const foundUrlsContain = div.getAttribute('data-found-urls-contain');
                                    return { url: url, category_id: categoryId, found_urls_contain: foundUrlsContain };
                                });
                                dataToSend[rowKey] = pageData.length > 0 ? pageData : [];
                            } else if (!arrayOfStringParameters.includes(rowKey)) {
                                const existingValues = [...document.querySelectorAll(`#${rowKey}-value div`)].map(div => div.getAttribute('data-value'));
                                dataToSend[rowKey] = existingValues.length > 0 ? existingValues : [];
                            }
                        });
                    }

                    function initializeSelectHandlers() {
                        const selectElements = document.querySelectorAll('#pages-container .form-control');
                        selectElements.forEach(select => {
                            select.addEventListener('change', function () {
                                updatePageData(select);
                            })
                        })
                    }

                    document.addEventListener('DOMContentLoaded', function () {
                        initializeDataToSend();
                        initializeSelectHandlers();
                    })
                </script>

            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@endsection
