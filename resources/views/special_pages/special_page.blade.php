@include('articles.header')
@include('partials.line_with_last_news')

<div class="container-fluid py-5">
    <div class="container py-5">
        <div class="row g-4">
            <div class="col-lg-8">

                @include('partials.breadcrumbs', ['specialPageTitle' => $specialPageTitle])

                <h1>{!! $specialPageTitle !!}</h1>

                @include('special_pages.content.' . $pageType . '_content')

                <div class="bg-light rounded my-4 p-4">

                    @include('partials.recommended_articles', ['recommendedArticles' => $recommendedArticles])

                </div>
            </div>
            <div class="col-lg-4">
                <div class="row g-4">
                    <div class="col-12">
                        <div class="p-3 rounded border">

                            @include('partials.popular_categories')
                            @include('partials.latest_articles')

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('articles.footer')
