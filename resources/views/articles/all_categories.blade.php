@php use App\Constants\NavigationConstants; @endphp
@include('articles.header', ['specialPageTitle' => NavigationConstants::ALL_CATEGORIES])
@include('partials.line_with_last_news')

<div class="container-fluid py-5">
    <div class="container py-5">
        <div class="row g-4">
            <div class="col-lg-8">
                <div class="sidebar">
                    @include('partials.breadcrumbs', ['isAllArticles' => true])
                    <div class="header-with-filters">
                        <h2 class="mb-3">{!! NavigationConstants::ALL_CATEGORIES !!}</h2>
                    </div>
                    <div class="category-list">
                        @foreach($categories as $category)
                            <div class="category-item">
                                <a href="{{ route('articles.filtered', ['category' => $category->slug]) }}"
                                   class="link">{{ $category->name }}</a>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="custom-pagination">
                    {{ $categories->links('partials.pagination_bootstrap_4') }}
                </div>

                <div class="bg-light rounded my-4 p-4">

                    @include('partials.recommended_articles', ['recommendedArticles' => $recommendedArticles])

                </div>
            </div>
            <div class="col-lg-4">
                <div class="row g-4">
                    <div class="col-12">
                        <div class="p-3 rounded border">
                            @include('partials.latest_articles')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('articles.footer')
