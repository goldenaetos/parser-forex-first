@php
    use App\Constants\NavigationConstants;

    $specialPageTitle = NavigationConstants::OUR_ARTICLES;
    if (isset($category)) {
        $specialPageTitle .= ' – ' . $category->name;
    }
@endphp
@include('articles.header', ['specialPageTitle' => $specialPageTitle])
@include('partials.line_with_last_news')

<div class="container-fluid py-5">
    <div class="container py-5">
        <div class="row g-4">
            <div class="col-lg-8">
                <div class="sidebar">
                    @include('partials.breadcrumbs', ['isAllArticles' => true])
                    <div class="header-with-filters">
                        <h2 class="mb-3">{!! NavigationConstants::OUR_ARTICLES !!}</h2>
                        <div class="filter-badges">
                            @if(isset($category))
                                <div class="filter-badge category-badge">
                                    {{ $category->name }}
                                    <a href="{{ route('articles.filtered', request()->except('category')) }}"
                                       class="remove-filter">&times;</a>
                                </div>
                            @endif
                            @if(request()->query('date'))
                                <div class="filter-badge date-badge">
                                    {{ request()->query('date') }}
                                    <a href="{{ route('articles.filtered', request()->except('date')) }}"
                                       class="remove-filter">&times;</a>
                                </div>
                            @endif
                        </div>
                        <select class="form-select" style="margin-bottom: 2rem" onchange="location = this.value;">
                            <option
                                value="{{ route('articles.filtered', ['date' => request()->query('date') ?? null]) }}">{!! NavigationConstants::ALL_CATEGORIES !!}</option>
                            @foreach($categories as $cat)
                                <option
                                    value="{{ route('articles.filtered', array_filter(['category' => $cat->slug, 'date' => request()->query('date') ?? null])) }}" {{ isset($category) && $category->slug === $cat->slug ? 'selected' : '' }}>{{ $cat->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <ul>
                        @php
                            $countArticles = count($articles);
                            $linkCount = 0;
                        @endphp
                        @foreach($articles as $article)
                            <li class="article-entry">
                                <a href="{{ route('articles.filtered', array_filter(['date' => $article->created_at->format('d-m-Y'), 'category' => $category->slug ?? null])) }}"
                                   class="article-date">({{ $article->created_at->format('d.m.Y') }})</a>
                                <a class="link-to-article"
                                   href="{{ route('articles.show', $article->slug) }}">{{ $article->title }}</a>
                            </li>
                            @php($linkCount++)

                            @if($linkCount === 20 && count($articles) > 24)
                                <div class="mt-4"></div>
                                @include('partials.middle_banner')
                            @endif
                        @endforeach
                    </ul>
                </div>

                @if($countArticles > 9 && $countArticles < 25)
                    <div class="mt-4"></div>
                    @include('partials.middle_banner')
                @endif

                <div class="custom-pagination">
                    {{ $articles->links('partials.pagination_bootstrap_4') }}
                </div>

                <div class="bg-light rounded my-4 p-4">

                    @include('partials.recommended_articles', ['recommendedArticles' => $recommendedArticles])

                </div>
            </div>
            <div class="col-lg-4">
                <div class="row g-4">
                    <div class="col-12">
                        <div class="p-3 rounded border">

                            @include('partials.popular_categories')
                            @include('partials.latest_articles')

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('articles.footer')
