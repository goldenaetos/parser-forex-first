@php
    use App\Constants\NavigationConstants;
    use Illuminate\Support\Facades\Request;

    $url = Request::url();
    $queryParams = Request::query();
@endphp

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>{!! $specialPageTitle ?? $article->title ?? NavigationConstants::GENERAL_SITE_TITLE !!}</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
@if(isset($queryParams['date']) && isset($queryParams['category']))
@php
    $canonical = $url . '?date=' . $queryParams['date'];
@endphp
    <link rel="canonical" href="{{ $canonical }}">
@endif

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600&family=Raleway:wght@100;600;800&display=swap"
        rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="{{ asset('general_content/lib/animate/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('general_content/lib/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="{{ asset('general_content/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="{{ asset('general_content/css/style.css') }}" rel="stylesheet">
</head>

<body>

<!-- Spinner Start -->
<div id="spinner"
     class="show w-100 vh-100 bg-white position-fixed translate-middle top-50 start-50  d-flex align-items-center justify-content-center">
    <div class="spinner-grow text-primary" role="status"></div>
</div>
<!-- Spinner End -->


<!-- Navbar start -->
<div class="container-fluid sticky-top px-0">
    <div class="container-fluid topbar bg-dark d-none d-lg-block">
        <div class="container px-0">
            <div class="topbar-top d-flex justify-content-between flex-lg-wrap">
                <div class="top-info flex-grow-0">
                            <span class="rounded-circle btn-sm-square bg-primary me-2">
                                <i class="fas fa-bolt text-white"></i>
                            </span>
                    <div class="pe-2 me-3 border-end border-white d-flex align-items-center">
                        <p class="mb-0 text-white fs-6 fw-normal">Trending</p>
                    </div>
                    <div class="overflow-hidden">
                        <a href="{{ route('recommendations') }}">
                            <div id="note" class="ps-2">
                                <img src="{{ asset('general_content/img/features-life-style.jpg') }}"
                                     class="img-fluid rounded-circle border border-3 border-primary me-2"
                                     style="width: 30px; height: 30px;" alt="">
                                <p class="text-white mb-0 link-hover">Оберіть найкращого брокера для торгівлі на фінансових ринках.</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="top-link flex-lg-wrap">
                    <i id="current-date" class="fas fa-calendar-alt text-white border-end border-secondary pe-2 me-2">
                        <span class="text-body"></span></i>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid bg-light">
        <div class="container px-0">
            <nav class="navbar navbar-light navbar-expand-xl">
                <a href="{{ route('home') }}" class="navbar-brand mt-3">
                    <p class="text-primary display-6 mb-2" style="line-height: 0;">&nbsp;&nbsp;Finance</p>
                    <small class="text-body fw-normal" style="letter-spacing: 12px;">Newspaper</small>
                </a>
                <button class="navbar-toggler py-2 px-3" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarCollapse">
                    <span class="fa fa-bars text-primary"></span>
                </button>
                <div class="collapse navbar-collapse bg-light py-3" id="navbarCollapse">
                    <div class="navbar-nav mx-auto border-top">
                        <a href="{{ route('home') }}" class="nav-item nav-link">Головна</a>
                        <a href="{{ route('articles') }}" class="nav-item nav-link">{!! NavigationConstants::OUR_ARTICLES !!}</a>
                        <a href="{{ route('partners') }}" class="nav-item nav-link">{!! NavigationConstants::PARTNERS !!}</a>
                        <a href="{{ route('recommendations') }}" class="nav-item nav-link">{!! NavigationConstants::RECOMMENDATIONS !!}</a>
                        <a href="{{ route('offer') }}" class="nav-item nav-link">{!! NavigationConstants::OFFER !!}</a>
                    </div>
                    <div class="d-flex flex-nowrap border-top pt-3 pt-xl-0">
                        <div class="d-flex">
                            <div class="me-2">
                                <img src="{{ asset('general_content/img/weather-icon.png') }}"
                                     class="img-fluid w-100 me-2" style="margin-top:5px">
                            </div>
                            <div class="d-flex align-items-center">
                                <div class="d-flex flex-column ms-2" style="width: 150px;">
                                    <span class="text-body">ALWAYS SUNNY</span>
                                    <small>Rich man's world</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>
<!-- Navbar End -->
