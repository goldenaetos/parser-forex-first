@php
    use App\Constants\NavigationConstants;
    use Illuminate\Support\Str;
@endphp

    <!-- Footer Start -->
<div class="container-fluid bg-dark footer py-5">
    <div class="container py-5">
        <div class="row g-5">
            <div class="col-lg-6 col-xl-3">
                <div class="footer-item-1">
                    <h4 class="mb-4 text-white">Наші переваги</h4>
                    <p class="line-h"><a class="footer-advantages-link" href="{{ route('partners') }}">{!! NavigationConstants::PARTNERS !!}</a></p>
                    <p class="line-h"><a class="footer-advantages-link" href="{{ route('recommendations') }}">{!! NavigationConstants::RECOMMENDATIONS !!}</a></p>
                    <p class="line-h"><a class="footer-advantages-link" href="{{ route('offer') }}">{!! NavigationConstants::OFFER !!}</a></p>
                    <div class="d-flex line-h">
                        <a class="btn btn-light me-2 btn-md-square rounded-circle" href="{{ route('articles') }}"
                           title="{!! NavigationConstants::OUR_ARTICLES !!}"><i
                                class="fas fa-book-open text-dark"></i></a>
                        <a class="btn btn-light me-2 btn-md-square rounded-circle" href="{{ route('partners') }}"
                           title="{!! NavigationConstants::PARTNERS !!}"><i
                                class="fas fa-glass-cheers text-dark"></i></a>
                        <a class="btn btn-light me-2 btn-md-square rounded-circle" href="{{ route('recommendations') }}"
                           title="{!! NavigationConstants::RECOMMENDATIONS !!}"><i
                                class="fas fa-user-tie text-dark"></i></a>
                        <a class="btn btn-light btn-md-square rounded-circle" href="{{ route('offer') }}"
                           title="{!! NavigationConstants::OFFER !!}"><i
                                class="fas fa-comment-dollar text-dark"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-xl-3">
                <div class="footer-item-2">
                    <a href="{{ route('articles') }}" class="footer-h4-link"><h4 class="mb-4">Останні публікації</h4>
                    </a>
                    @foreach($latestNews as $news)
                        <div class="d-flex flex-column mb-4">
                            <a href="javascript:void(0);">
                                <div class="d-flex align-items-center">
                                    <div class="rounded-circle border border-2 border-primary overflow-hidden">
                                        @php
                                            $previewPath = $news->small_preview;
                                            if (!$previewPath || !file_exists($previewPath)) {
                                                $previewPath = $mockupImages->shift();
                                            }
                                        @endphp
                                        <img src="{{ asset($previewPath) }}"
                                             class="img-zoomin img-fluid rounded-circle w-100">
                                    </div>
                                    <div class="d-flex flex-column ps-4">
                                        @if($news->category)
                                            <a href="{{ route('articles.filtered', ['category' => $news->category->slug]) }}"
                                               class="text-uppercase text-white mb-3">{{ $news->category->name }}</a>
                                        @endif
                                        <a href="{{ route('articles.show', ['slug' => $news->slug]) }}"
                                           class="h6 text-white">
                                            {{ Str::limit($news->title, 29) }}
                                        </a>
                                        <small class="text-white d-block"><i
                                                class="fas fa-calendar-alt me-1"></i>{{ $news->created_at->format('M d, Y') }}
                                        </small>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-lg-6 col-xl-3">
                <div class="d-flex flex-column text-start footer-item-3">
                    <a href="{{ route('articles.all_categories') }}" class="footer-h4-link"><h4 class="mb-4">
                            Категорії</h4></a>
                    @foreach($popularCategories as $category)
                        <a href="{{ route('articles.filtered', ['category' => $category->slug]) }}"
                           class="btn-link text-white"><i
                                class="fas fa-angle-right text-white me-2"></i> {{ $category->name }}</a>
                    @endforeach
                </div>
            </div>
            <div class="col-lg-6 col-xl-3">
                <div class="footer-item-4">
                    <h4 class="mb-4 text-white">Наша галерея</h4>
                    <div class="row g-2">
                        @foreach($randomArticlesForGallery as $article)
                            <div class="col-4">
                                @php
                                    $previewPath = $article->preview;
                                    if (!$previewPath || !file_exists(public_path($previewPath))) {
                                        $previewPath = $mockupImages->shift();
                                    }
                                @endphp
                                <a href="{{ url('/articles/' . $article->slug) }}" class="rounded overflow-hidden">
                                    <img src="{{ asset($previewPath) }}" class="img-zoomin img-fluid rounded w-100">
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer End -->

<!-- Back to Top -->
<a href="#" class="btn btn-primary border-2 border-white rounded-circle back-to-top"><i class="fa fa-arrow-up"></i></a>

<!-- JavaScript Libraries -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
<script src="{{ asset('general_content/lib/easing/easing.min.js') }}"></script>
<script src="{{ asset('general_content/lib/waypoints/waypoints.min.js') }}"></script>
<script src="{{ asset('general_content/lib/owlcarousel/owl.carousel.min.js') }}"></script>

<!-- Template Javascript -->
<script src="{{ asset('general_content/js/main.js') }}"></script>
<script src="{{ asset('general_content/js/custom.js') }}"></script>

<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-TYBSHZGKQ5"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-TYBSHZGKQ5');
</script>

<!-- Clarity -->
<script type="text/javascript">
    (function(c,l,a,r,i,t,y){
        c[a]=c[a]||function(){(c[a].q=c[a].q||[]).push(arguments)};
        t=l.createElement(r);t.async=1;t.src="https://www.clarity.ms/tag/"+i;
        y=l.getElementsByTagName(r)[0];y.parentNode.insertBefore(t,y);
    })(window, document, "clarity", "script", "m8sjwe2osp");
</script>
</body>

</html>
