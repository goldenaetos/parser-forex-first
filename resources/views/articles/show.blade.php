@include('articles.header')
@include('partials.line_with_last_news')

<div class="container-fluid py-5">
    <div class="container py-5">
        <div class="row g-4">
            <div class="col-lg-8">

                @include('partials.breadcrumbs', ['category' => $article->category, 'article' => $article])

                <span style="color: grey; font-size: small;">{{ $article->created_at->format('d.m.Y') }}</span>

                @php
                    $contentArray = json_decode($article->content, true);
                    $consecutiveTextCount = 0;
                    $specialBlockInserted = false;
                    $firstImageShown = false;
                @endphp

                @foreach($contentArray as $item)
                    @if($item['type'] === 'heading')
                        @if($item['level'] === 1)
                            <div class="mb-4">
                                <h1>{{ $item['content'] }}</h1>
                            </div>
                        @else
                            <h{{ $item['level'] }}>{{ $item['content'] }}</h{{ $item['level'] }}>
                        @endif
                    @elseif($item['type'] === 'text')
                        <p class="my-4">{{ $item['content'] }}</p>
                        @if($consecutiveTextCount < 9 && $item['content'] !== "")
                            @php $consecutiveTextCount++; @endphp
                        @endif
                    @elseif($item['type'] === 'image')
                        <div class="position-relative rounded overflow-hidden mb-3">
                            <img src="{{ asset($item['content']) }}" class="img-zoomin img-fluid rounded w-100">
                            @if(!$firstImageShown && $article->category)
                                <div class="position-absolute text-white px-4 py-2 bg-primary rounded" style="top: 20px; right: 20px;">
                                    <a href="{{ route('articles.filtered', ['category' => $article->category->slug]) }}" class="text-white">{{ $article->category->name }}</a>
                                </div>
                                @php
                                    $firstImageShown = true;
                                @endphp
                            @endif
                        </div>
                        @php $consecutiveTextCount = 0; @endphp
                    @elseif($item['type'] === 'link')
                        <a href="{{ $item['content'] }}" target="_blank">{{ $item['content'] }}</a>
                        @php $consecutiveTextCount = 0; @endphp
                    @endif

                    @if($consecutiveTextCount === 9 && !$specialBlockInserted)

                        @include('partials.middle_banner')

                        @php
                            $specialBlockInserted = true;
                            $consecutiveTextCount = 0;
                        @endphp
                    @endif
                @endforeach
                <div class="bg-light rounded my-4 p-4">

                    @include('partials.recommended_articles', ['recommendedArticles' => $recommendedArticles])

                </div>
            </div>
            <div class="col-lg-4">
                <div class="row g-4">
                    <div class="col-12">
                        <div class="p-3 rounded border">

                            @include('partials.popular_categories')
                            @include('partials.latest_articles')

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('articles.footer')
