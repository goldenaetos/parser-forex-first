<h4 class="mb-4 center">Останні новини</h4>
<div class="row g-4">
    @foreach ($latestArticles as $article)
        <div class="col-12">
            <div class="row g-4 align-items-center features-item">
                <div class="col-4">
                    <div class="rounded-circle position-relative">
                        <div class="overflow-hidden image-container rounded-circle">
                            @php
                                $previewPath = $article->preview;
                                if (!$previewPath || !file_exists(public_path($previewPath))) {
                                    $previewPath = $mockupImages->shift();
                                }
                            @endphp
                            <img src="{{ asset($previewPath) }}" class="img-zoomin img-fluid rounded-circle w-100">
                        </div>
                    </div>
                </div>
                <div class="col-8">
                    <div class="features-content d-flex flex-column">

                        @if($article->category)
                        <p class="text-uppercase mb-2">
                            <a href="{{ route('articles.filtered', ['category' => $article->category->slug]) }}">{{ $article->category->name }}</a>
                        </p>
                        @endif

                        <a href="{{ route('articles.show', $article->slug) }}" class="h6">
                            {{ $article->title }}
                        </a>
                        <small class="text-body d-block"><i class="fas fa-calendar-alt me-1"></i> {{ $article->created_at->format('F d, Y') }}</small>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    <div class="col-lg-12 view-more">
        <a href="{{ route('articles') }}" class="link-hover btn border border-primary rounded-pill text-dark w-100 py-3 mb-1">Дивитись більше</a>
    </div>

    @include('partials.right_bottom_banner')

</div>
