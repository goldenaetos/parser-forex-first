@php
    use Illuminate\Support\Str;
@endphp

@if($recommendedArticles->isNotEmpty())
    <h4 class="mb-3">Вам також може сподобатись</h4>
    <div class="row g-4">
        @foreach($recommendedArticles as $recArticle)
            <div class="col-lg-6">
                <div class="d-flex align-items-start p-3 bg-white rounded">
                    @php
                        $previewPath = $recArticle->preview;
                        if (!$previewPath || !file_exists(public_path($previewPath))) {
                            $previewPath = $mockupImages->shift();
                        }
                    @endphp
                    <img src="{{ asset($previewPath) }}" class="img-fluid rounded" width="100px" height="100px">
                    <div class="ms-3">
                        <a href="{{ route('articles.show', $recArticle->slug) }}"
                           class="h5 mb-2">{{ Str::limit($recArticle->title, 57) }}</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endif
