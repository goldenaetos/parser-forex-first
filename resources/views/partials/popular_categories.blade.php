<h4 class="mb-4 center">Популярні категорії</h4>
<div class="row g-2 mb-4">
    @foreach ($popularCategories as $category)
        <div class="col-12">
            <a href="{{ route('articles.filtered', ['category' => $category->slug]) }}" class="link-hover btn btn-light w-100 rounded text-uppercase text-dark py-3">
                {{ $category->name }}
            </a>
        </div>
    @endforeach
</div>
