@php
    use \Illuminate\Support\Str;
@endphp

<!-- Features Start -->
<div class="container-fluid features">
    <div class="container py-5">
        <div class="row g-4">
            @foreach($uniqueCategoryArticles as $index => $article)
                <div class="col-md-6 col-lg-6 col-xl-3 {{ $index >= 2 ? 'hide-on-mobile' : '' }}">
                    <div class="row g-4 align-items-center features-item">
                        <div class="col-4">
                            <div class="rounded-circle position-relative">
                                <div class="overflow-hidden rounded-circle">
                                    @php
                                        $previewPath = $article->preview;
                                        if (!$previewPath || !file_exists(public_path($previewPath))) {
                                            $previewPath = $mockupImages->shift();
                                        }
                                    @endphp
                                    <img src="{{ asset($previewPath) }}" class="img-zoomin img-fluid rounded-circle w-100">
                                </div>
                            </div>
                        </div>
                        <div class="col-8">
                            <div class="features-content d-flex flex-column">
                                @if($article->category)
                                    <p class="text-uppercase mb-2">
                                        <a href="{{ route('articles.filtered', ['category' => $article->category->slug]) }}">{{ $article->category->name ?? ' ' }}</a>
                                    </p>
                                @else
                                    <p class="text-uppercase mb-2">
                                        <a href="#">&nbsp;</a>
                                    </p>
                                @endif
                                <a href="{{ route('articles.show', $article->slug) }}" class="h6">
                                    {{ Str::limit($article->title, 49) }}
                                </a>
                                <small class="text-body d-block"><i class="fas fa-calendar-alt me-1"></i> {{ $article->created_at->format('F d, Y') }}</small>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
<!-- Features End -->
