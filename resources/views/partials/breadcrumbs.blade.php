@php
    use App\Constants\NavigationConstants;$date = request()->query('date') ?? null;

    $part1 = '<a href="' . route('home') . '">Головна</a>';

    if (isset($category) && !isset($article)) {
        if (!$date) {
            $part2 = ' / ' . $category->name;
        } else {
            $part2 = ' / <a href="' . route('articles.filtered', ['category' => $category->slug]) . '">' . $category->name . '</a> / ' . $date;
        }

    } elseif (isset($category) && isset($article)) {
        $part2 = ' / <a href="' . route('articles.filtered', ['category' => $category->slug]) . '">' . $category->name . '</a>';
    } elseif (!isset($category) && isset($article)) {
        $part2 = ' / <a href="' . route('articles') . '">' . NavigationConstants::OUR_ARTICLES . '</a>';
    } elseif (isset($specialPageTitle)) {
        $part2 = ' / ' . $specialPageTitle;
    } elseif (isset($isAllArticles)) {
        if (!$date) {
        $part2 = ' / ' . NavigationConstants::OUR_ARTICLES;
        } else {
            $part2 = ' / <a href="' . route('articles') . '">' . NavigationConstants::OUR_ARTICLES . '</a> / ' . $date;
        }
    } else {
        $part2 = "";
    }

    if (isset($article)) {
        $part3 = ' / ' . $article->title;
    } else {
        $part3 = "";
    }
@endphp

<div class="custom-breadcrumbs">{!! $part1 !!}{!! $part2 !!}{!! $part3 !!}</div>
