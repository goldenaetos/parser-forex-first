<div class="col-lg-12">
    <div class="link-block position-relative banner-2" data-href="https://gobymylink.com/en/?partner_id=90187">
        <img src="{{ asset('general_content/img/banner-2.jpg') }}" class="img-fluid w-100 rounded">
        <div class="text-center banner-content-2">
            <h6 class="mb-2"><span class="disappearing-text">Найкращий брокер</span>&nbsp;</h6>
            <p class="text-white mb-2"><span class="disappearing-text">для торгівлі та інвестицій</span>&nbsp;</p>
            <div class="btn btn-primary text-white px-4">Дізнатись більше!</div>
        </div>
    </div>
</div>

