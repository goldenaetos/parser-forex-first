<div class="link-block" data-href="https://track.deriv.com/_oRAedO2E8Paws6I0Pc5-VWNd7ZgqdRLk/1/">
    <div class="bg-light p-4 border-start border-top border-end border-3 border-primary" style="margin-bottom: 1.5rem !important;">
        <h2 class="mb-2 center middle-banner-h2">Отримай доступ до всіх ринків</h2>
    </div>
    <div class="row g-4">
        <div class="col-12">
            <div class="rounded overflow-hidden">
                <img src="{{ asset('banners/deriv970x250.png') }}" class="img-zoomin img-fluid rounded w-100">
            </div>
        </div>
    </div>
    <div class="bg-light p-4 border-start border-bottom border-end border-3 border-primary" style="margin-bottom: 1.5rem !important;">
        <h2 class="mb-2 center middle-banner-h2">з єдиної платформи від Deriv</h2>
    </div>
</div>
