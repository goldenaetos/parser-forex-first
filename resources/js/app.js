require('./bootstrap');

import { createApp } from 'vue';
import WeatherApp from './components/WeatherApp.vue';

createApp(WeatherApp).mount('#weather-app');
