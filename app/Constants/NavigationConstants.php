<?php

namespace App\Constants;

class NavigationConstants
{
    const GENERAL_SITE_TITLE = 'У&nbsp;світі&nbsp;фінансів';
    const ALL_CATEGORIES = 'Всі&nbsp;категорії';
    const OUR_ARTICLES = 'Наші&nbsp;статті';
    const RECOMMENDATIONS = 'Наші&nbsp;рекомендації';
    const PARTNERS = 'Наші&nbsp;партнери';
    const OFFER = 'Наша&nbsp;пропозиція';
}
