<?php

namespace App\Constants;

use ReflectionClass;

class NamesForWebsiteSettingsParameters
{
    const NAME = 'Название комплекта';
    const WEBSITE_ADDRESS = 'Адрес сайта';
    const PAGES = 'Страницы';
    const STRING_STARTS = 'Строка начинается с';
    const STRING_ENDS = 'Строка заканчивается';
    const PATTERN_FOR_DETECT_URLS = 'Паттерн для определения URLов';
    const INAPPROPRIATE_PAGES = 'Части URLов, которые исключить';
    const INAPPROPRIATE_TAG = 'Тег, который исключить (h1-h6)';
    const EXCLUDE_CLASS_FOR_H = 'Исключить h1-h6 с классами';
    const EXCLUDE_CLASS_FOR_P = 'Исключить p с классами';
    const EXCLUDE_CLASS_FOR_IMG = 'Исключить img с классами';
    const STOP_WORDS = 'Стоп-слова, дальше которых не продолжать парсинг';
    const TEXT_EQUALS = 'Абзацы, которые исключить (полное совпадение)';
    const TEXT_STARTS_WITH = 'Исключить абзацы, начинающиеся с';
    const TEXT_STARTS_WITH_PATTERN = 'Исключить абзацы, начинающиеся с (шаблоны)';
    const TEXT_CONTAINS = 'Исключить абзацы, содержащие';
    const PATH_TO_IMAGE_CONTAINS = 'Исключить картинки, содержащие в адресе';
    const REMOVE_IMAGE_PARAMS_PATTERNS = 'Исключить параметры картинок (вхождение; дополнительное вхождение; паттерн)';
    const IMAGE_TAIL = 'Хвост изображения дописать (напр., ?width=1500&dpr=1&s=none)';
    const CREATED_AT = 'Комплект добавлен';
    const UPDATED_AT = 'Последний раз редактировалось';

    public static function getAllConstantsToLowerCaseWithValues(): array
    {
        $reflection = new ReflectionClass(self::class);

        $constants = $reflection->getConstants();

        return array_change_key_case($constants);
    }
}
