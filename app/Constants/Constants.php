<?php

namespace App\Constants;

class Constants
{
    const LAST_UPDATED = 'Изменено';

    const EXCLUDED_KEYS_FOR_SHOWING = [
        'deleted_at'
    ];

    const EXCLUDED_KEYS_FOR_EDITING = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    const STRING_PARAMETERS = [
        'name',
        'website_address',
        'inappropriate_tag',
        'string_starts',
        'string_ends',
        'pattern_for_detect_urls',
        'image_tail'
    ];

    const TARGET_LANGUAGE = 'uk';
}
