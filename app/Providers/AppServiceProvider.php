<?php

namespace App\Providers;

use App\Models\Article;
use App\Models\Category;
use App\Services\ArticlesService;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->composePopularCategories();
        $this->composeLatestArticles();
        $this->composeRecommendedArticlesImages();
        $this->composeLineWithLastNews();
        $this->composeGallery();
        $this->composeFooterNews();
        $this->composeUniqueCategoryArticles();
        $this->composeRecommendedArticles();
    }

    private function composePopularCategories()
    {
        View::composer(['partials.popular_categories', 'articles.footer'], function ($view) {
            $view->with('popularCategories', Category::withCount('articles')
                ->orderBy('articles_count', 'desc')
                ->orderBy('name', 'asc')
                ->take(6)
                ->get());
        });
    }

    private function composeLatestArticles()
    {
        View::composer('partials.latest_articles', function ($view) {
            $view->with([
                'latestArticles' => Article::with('category')
                    ->orderBy('created_at', 'desc')
                    ->take(5)
                    ->get(),
                'mockupImages' => $this->getMockupImages()
            ]);
        });
    }

    private function composeRecommendedArticlesImages()
    {
        View::composer('partials.recommended_articles', function ($view) {
            $view->with('mockupImages', $this->getMockupImages());
        });
    }

    private function composeLineWithLastNews()
    {
        View::composer('partials.line_with_last_news', function ($view) {
            $view->with('mockupImages', $this->getMockupImages());
        });
    }

    private function composeGallery()
    {
        View::composer('articles.footer', function ($view) {
            $randomArticles = Article::inRandomOrder()
                ->take(6)
                ->get(['preview', 'slug']);
            $mockupImages = $this->getMockupImages();
            $view->with([
                'randomArticlesForGallery' => $randomArticles,
                'mockupImages' => $mockupImages
            ]);
        });
    }

    private function composeFooterNews()
    {
        View::composer('articles.footer', function ($view) {
            $latestNews = Article::with('category')
                ->latest('created_at')
                ->take(2)
                ->orderBy('created_at', 'desc')
                ->get();
            $mockupImages = $this->getMockupImages('_small');
            $view->with([
                'latestNews' => $latestNews,
                'mockupImages' => $mockupImages
            ]);
        });
    }

    private function getMockupImages($tail = null): Collection
    {
        $mockupImagePath = public_path(config('paths.mockup_images')) . $tail;
        return collect(File::allFiles($mockupImagePath))->map(function ($file) use ($tail) {
            return config('paths.mockup_images') . $tail . '/' . $file->getFilename();
        })->shuffle();
    }

    private function composeUniqueCategoryArticles()
    {
        View::composer(['articles.index', 'articles.all_articles', 'articles.show', 'articles.all_categories', 'special_pages.special_page'], function ($view) {
            $uniqueCategoryArticles = app(ArticlesService::class)->getUniqueCategoryArticles();
            $view->with('uniqueCategoryArticles', $uniqueCategoryArticles);
        });
    }

    private function composeRecommendedArticles()
    {
        View::composer(['articles.index', 'articles.all_articles', 'articles.all_categories', 'special_pages.special_page'], function ($view) {
            $recommendedArticles = app(ArticlesService::class)->getRecommendedArticles();
            $view->with('recommendedArticles', $recommendedArticles);
        });
    }
}
