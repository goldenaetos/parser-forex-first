<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Page extends Model
{
    use HasFactory;

    protected $table = 'pages';

    protected $fillable = [
        'url',
        'found_urls_contain',
        'set_of_website_id',
        'category_id'
    ];

    public function setOfWebsite(): BelongsTo
    {
        return $this->belongsTo(SetsOfWebsite::class, 'set_of_website_id');
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}
