<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class SetsOfWebsite extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'sets_of_websites';

    protected $fillable = [
        'name',
        'website_address',
        'string_starts',
        'string_ends',
        'pattern_for_detect_urls',
        'inappropriate_pages',
        'inappropriate_tag',
        'exclude_class_for_h',
        'exclude_class_for_p',
        'exclude_class_for_img',
        'pattern_for_parsing_titles',
        'stop_words',
        'text_equals',
        'text_starts_with',
        'text_starts_with_pattern',
        'text_contains',
        'path_to_image_contains',
        'remove_image_params_patterns',
        'image_tail'
    ];

    public function pages(): HasMany
    {
        return $this->hasMany(Page::class, 'set_of_website_id');
    }
}
