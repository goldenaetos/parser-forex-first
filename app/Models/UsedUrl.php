<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsedUrl extends Model
{
    use HasFactory;

    protected $table = 'used_urls';

    protected $fillable = [
        'url',
        'page'
    ];
}
