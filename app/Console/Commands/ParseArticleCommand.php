<?php

namespace App\Console\Commands;

use App\Http\Controllers\ParserController;
use App\Services\ParserService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ParseArticleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:article';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parses the article, translates it and saves it into the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(ParserService $parserService)
    {
        try {
            $parserController = new ParserController($parserService);
            $parserController->parseArticle();
            $this->line('<fg=green>The article was parsed successfully');
        } catch (\Exception $e) {
            Log::error('[' . date('Y-m-d H:i:s') . '] ParseArticleCommand error: ' . $e->getMessage());
            $this->error('Error: ' . $e->getMessage());
        }
    }
}
