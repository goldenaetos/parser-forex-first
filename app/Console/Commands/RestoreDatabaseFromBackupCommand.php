<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class RestoreDatabaseFromBackupCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:restore {file : The path to the backup file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Restore the database from a backup file';

    /**
     * Execute the console command.
     *
     * @return int|void
     */
    public function handle()
    {
        $backupFile = $this->argument('file');

        if (!File::exists($backupFile)) {
            $this->error('Backup file not found');
            return;
        }

        $dbName = config('database.connections.mysql.database');
        $dbUser = config('database.connections.mysql.username');
        $dbPassword = config('database.connections.mysql.password');

        $command = sprintf(
            'mysql -u %s -p%s %s < %s',
            escapeshellarg($dbUser),
            escapeshellarg($dbPassword),
            escapeshellarg($dbName),
            escapeshellarg($backupFile)
        );

        exec($command);

        $this->info('Database successfully restored from backup.');
    }
}
