<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

class CreateDatabaseBackupCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a backup of the database';

    /**
     * Execute the console command.
     *
     * @return int|void
     */
    public function handle()
    {
        $backupDir = base_path('backups');
        $currentBasePath = base_path();

        $domainName = basename($currentBasePath);
        $isLocalServer = strpos($domainName, '.') === false;
        if ($isLocalServer) {
            $domainName = 'localhost';
        }

        $backupFile = $backupDir . '/' . $domainName . '_' . now()->format('YmdHis') . '.sql';

        if (!File::exists($backupDir)) {
            File::makeDirectory($backupDir, 0755, true);
        }

        $dbUser = config('database.connections.mysql.username');
        $dbName = config('database.connections.mysql.database');

        if ($isLocalServer) {
            $dbPassword = config('database.connections.mysql.password');
            $dbHost = config('database.connections.mysql.host');
            $command = sprintf(
                '/usr/bin/mysqldump -h %s -u %s -p%s %s > %s',
                escapeshellarg($dbHost),
                escapeshellarg($dbUser),
                escapeshellarg($dbPassword),
                escapeshellarg($dbName),
                escapeshellarg($backupFile)
            );
        } else {
            $configFile = escapeshellarg('~/.my.cnf');
            $command = sprintf(
                '/usr/local/mysql/bin/mysqldump --defaults-file=%s -u %s %s > %s',
                $configFile,
                escapeshellarg($dbUser),
                escapeshellarg($dbName),
                escapeshellarg($backupFile)
            );
        }

        exec($command, $output, $returnVar);

        if ($returnVar === 0) {
            exec('chown www-data:www-data ' . escapeshellarg($backupFile));
            $this->info('Database backup created successfully');
            $this->deleteOldBackups($backupDir);
        } else {
            Log::error('Database backup creation failed');
            $this->error('Database backup creation failed');
        }
    }

    /**
     * Delete old backups to maintain a maximum of 10 backup files.
     *
     * @param string $backupDir The directory where backups are stored.
     */
    private function deleteOldBackups(string $backupDir)
    {
        $files = collect(File::files($backupDir))
            ->sortByDesc(function ($file) {
                return $file->getMTime();
            })
            ->skip(10);

        foreach ($files as $file) {
            File::delete($file);
            $this->line('Deleted old backup: ' . $file->getFilename());
        }
    }
}
