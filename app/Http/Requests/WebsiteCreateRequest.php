<?php

namespace App\Http\Requests;

use Exception;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class WebsiteCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'string_starts' => 'nullable|string|max:2500',
            'string_ends' => 'nullable|string|max:2500',
            'pattern_for_detect_urls' => 'nullable|string|max:2500',
            'inappropriate_pages' => 'nullable|json',
            'pages.*.found_urls_contain' => 'nullable|string',
            'inappropriate_tag' => 'nullable|string|max:2',
            'exclude_class_for_h' => 'nullable|json',
            'exclude_class_for_p' => 'nullable|json',
            'exclude_class_for_img' => 'nullable|json',
            'stop_words' => 'nullable|json',
            'text_equals' => 'nullable|json',
            'text_starts_with' => 'nullable|json',
            'text_starts_with_pattern' => 'nullable|json',
            'text_contains' => 'nullable|json',
            'path_to_image_contains' => 'nullable|json',
            'remove_image_params_patterns' => 'nullable|json',
            'remove_image_params_patterns.*.occurrence' => 'required_with:remove_image_params_patterns.*.occurrence|required_with:remove_image_params_patterns.*.pattern',
            'remove_image_params_patterns.*.occurrence_additional' => 'nullable',
            'remove_image_params_patterns.*.pattern' => 'required_with:remove_image_params_patterns.*.occurrence|required_with:remove_image_params_patterns.*.occurrence_additional',
            'image_tail' => 'nullable|string|max:2500'
        ];

        $setId = $this->route('id');
        $nameRule = 'string|max:2500|unique:sets_of_websites,name';
        if ($this->isMethod('put') || $this->isMethod('patch')) {
            $nameRule = Rule::unique('sets_of_websites', 'name')->ignore($setId);
        }
        $rules['name'] = $this->isMethod('put') || $this->isMethod('patch') ? ['string', 'max:2500', $nameRule] : 'required|string|unique:sets_of_websites,name|max:2500';
        $rules['website_address'] = $this->isMethod('post') ? 'required|string|max:2500' : 'string|max:2500';
        $rules['pages.*.url'] = $this->isMethod('post') ? 'required|string' : 'string';
        $rules['pages.*.category_id'] = $this->isMethod('post') ? 'required|integer' : 'integer';

        return $rules;
    }

    /**
     * @throws Exception
     */
    protected function failedValidation(Validator $validator)
    {
        Log::error('Validation failed: ' . $validator->errors());
        throw new ValidationException($validator, response()->json(['error' => $validator->errors()], Response::HTTP_UNPROCESSABLE_ENTITY));
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'inappropriate_pages' => $this->convertToJson($this->input('inappropriate_pages')),
            'exclude_class_for_h' => $this->convertToJson($this->input('exclude_class_for_h')),
            'exclude_class_for_p' => $this->convertToJson($this->input('exclude_class_for_p')),
            'exclude_class_for_img' => $this->convertToJson($this->input('exclude_class_for_img')),
            'stop_words' => $this->convertToJson($this->input('stop_words')),
            'text_equals' => $this->convertToJson($this->input('text_equals')),
            'text_starts_with' => $this->convertToJson($this->input('text_starts_with')),
            'text_starts_with_pattern' => $this->convertToJson($this->input('text_starts_with_pattern')),
            'text_contains' => $this->convertToJson($this->input('text_contains')),
            'path_to_image_contains' => $this->convertToJson($this->input('path_to_image_contains')),
            'remove_image_params_patterns' => $this->convertToJson($this->input('remove_image_params_patterns')),
        ]);
    }

    protected function convertToJson($value)
    {
        if (is_array($value)) {
            return json_encode($value, JSON_THROW_ON_ERROR);
        }
        return $value;
    }
}
