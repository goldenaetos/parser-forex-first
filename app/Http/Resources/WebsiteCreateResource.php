<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WebsiteCreateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'website_address' => $this->website_address,
            'string_starts' => $this->string_starts,
            'string_ends' => $this->string_ends,
            'pattern_for_detect_urls' => $this->pattern_for_detect_urls,
            'inappropriate_pages' => $this->inappropriate_pages,
            'inappropriate_tag' => $this->inappropriate_tag,
            'exclude_class_for_h' => $this->exclude_class_for_h,
            'exclude_class_for_p' => $this->exclude_class_for_p,
            'exclude_class_for_img' => $this->exclude_class_for_img,
            'pattern_for_parsing_titles' => $this->pattern_for_parsing_titles,
            'stop_words' => $this->stop_words,
            'text_equals' => $this->text_equals,
            'text_starts_with' => $this->text_starts_with,
            'text_starts_with_pattern' => $this->text_starts_with_pattern,
            'text_contains' => $this->text_contains,
            'path_to_image_contains' => $this->path_to_image_contains,
            'remove_image_params_patterns' => $this->remove_image_params_patterns,
            'image_tail' => $this->image_tail,
            'created_at' => $this->created_at,
            'updated_at' => $this->apdated_at
        ];
    }
}
