<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckAuthSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::check() && $request->session()->has('auth_time')) {
            $authTime = $request->session()->get('auth_time');
            $lastActionTime = $request->session()->get('last_action_time', now());
            $currentTime = now();

            if ($currentTime->diffInHours($authTime) >= 4 || $currentTime->diffInMinutes($lastActionTime) >= 60) {
                Auth::logout();
                $request->session()->invalidate();
                $request->session()->regenerateToken();
                return redirect()->route('home')->withErrors(['message' => 'Your session has expired due to inactivity. Please login again.']);
            } else {
                $request->session()->put('last_action_time', $currentTime);
            }
        }

        return $next($request);
    }
}
