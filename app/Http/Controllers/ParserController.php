<?php

namespace App\Http\Controllers;
use App\Models\Page;
use App\Models\SetsOfWebsite;
use App\Services\ParserService;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class ParserController extends Controller
{
    protected $parserService;
    protected $methodIds;

    public function __construct(ParserService $parserService)
    {
        $this->parserService = $parserService;
    }

    public function parseArticle($methodIds = [])
    {
        if ($methodIds === 'Empty') {
            return response()->json(['error_message' => 'All sites for parsing ended!'], Response::HTTP_NO_CONTENT);
        }

        if ($methodIds === []) {
            $setsOfWebsites = SetsOfWebsite::all();
            foreach ($setsOfWebsites as $set) {
                $methodIds[] = $set->id;
            }
        }

        $this->methodIds = $methodIds;

        if (!$methodIds) {
            return response()->json(['error_message' => "There are no working methods (web-sites) left!"], Response::HTTP_NO_CONTENT);
        }

        $selectedWebsiteId = $methodIds[array_rand($methodIds)];

        $this->parseArticles($selectedWebsiteId);
    }

    protected function parseArticles($id, $urls = [])
    {
        $setsOfWebsite = SetsOfWebsite::findOrFail($id);

        $stringStart = $setsOfWebsite->string_starts;
        $stringEnd = $setsOfWebsite->string_ends;
        $pattern = $setsOfWebsite->pattern_for_detect_urls;
        $imageTail = $setsOfWebsite->image_tail;
        $urlsOriginalArray = json_decode($setsOfWebsite->pages, true);
        $urlsOriginal = array_column($urlsOriginalArray, 'url');

        if ($urls === []) {
            $urls = $urlsOriginal;
        }

        if ($urls === 'empty') {
            $this->methodIds = $this->parserService->handleEmptyUrls($id, $this->methodIds);
            return $this->parseArticle($this->methodIds);
        }

        $url = $urls[array_rand($urls)];

        $page = Page::where('url', $url)->first();

        $categoryId = optional($page)->category_id;

        $uniquePages = $this->parserService->detectUniquePages($id, $url, $page, $stringStart, $stringEnd, $pattern);

        if ($uniquePages) {
            $singlePage = $this->parserService->detectSinglePage($uniquePages);

            try {
                $title = $this->parserService->parseArticlesTitle($singlePage);
                $content = $this->parserService->parseArticlesContent($singlePage, $id, $title, $imageTail);
                $this->parserService->saveArticle($title, $content, $categoryId, $imageTail);
                $this->parserService->saveUrlOfSinglePage($url, $singlePage);
            } catch (\Exception $e) {
                Log::error('Error with URL: ' . $url . ' during process single page: ' . $singlePage, [
                    'error_message' => $e->getMessage()
                ]);
                $this->parserService->saveUrlOfSinglePage($url, $singlePage);
                return $this->parseArticle();
            }
        }

        if (!$uniquePages) {
            $urls = $this->parserService->handleEmptyUniquePages($url, $urls);
            $this->parseArticles($id, $urls);
        }
    }
}
