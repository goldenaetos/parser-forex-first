<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Traits\Transliterable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class CategoryController extends Controller
{
    use Transliterable;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('check.auth.session');
    }

    public function index()
    {
        $categories = Category::orderBy('name')->get();

        $h1 = 'Список категорий';
        $title = 'Список категорий';
        $breadcrumbs = [
            ['title' => 'Admin Panel', 'route' => route('admin')],
            ['title' => 'Список категорий', 'route' => null]
        ];

        return view('admin.categories.all_categories', compact('categories', 'h1', 'title', 'breadcrumbs'));
    }

    public function create()
    {
        $h1 = 'Создать категорию';
        $breadcrumbs = [
            ['title' => 'Admin Panel', 'route' => route('admin')],
            ['title' => 'All Categories', 'route' => route('categories.index')],
            ['title' => 'Create Category', 'route' => null]
        ];

        return view('admin.categories.create', compact('h1', 'breadcrumbs'));
    }

    public function store(Request $request): JsonResponse
    {
        try {
            $validatedData = $request->validate([
                'name' => 'required|unique:categories'
            ]);

            $slug = Str::slug($this->transliterable($request->name), '-');

            $validatedData['slug'] = $slug;

            $category = Category::create($validatedData);

            return response()->json(['success' => true, 'category' => $category]);
        } catch (ValidationException $e) {
            Log::info($e->errors());
            return response()->json(['success' => false, 'errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return response()->json(['success' => false, 'error' => $e->getMessage()], 500);
        }
    }

    public function show(Category $category)
    {
        $id = $category->id;
        $h1 = $title = $category->name;
        $breadcrumbs = [
            ['title' => 'Admin Panel', 'route' => route('admin')],
            ['title' => 'All Categories', 'route' => route('categories.index')],
            ['title' => $category->name, 'route' => null]
        ];

        return view('admin.categories.show', compact('category', 'h1', 'title', 'breadcrumbs', 'id'));
    }

    public function edit(Category $category)
    {
        return view('categories.edit', compact('category'));
    }

    public function update(Request $request, Category $category): JsonResponse
    {
        try {
            $validatedData = $request->validate([
                'name' => 'required|unique:categories,name,' . $category->id
            ]);

            $slug = Str::slug($this->transliterable($request->name), '-');

            $slugExists = Category::where('slug', $slug)->where('id', '!=', $category->id)->exists();

            if ($slugExists) {
                return response()->json(['success' => false, 'message' => 'The category name generates a slug that already exists.'], 422);
            }

            $validatedData['slug'] = $slug;

            $category->update($validatedData);

            return response()->json(['success' => true, 'message' => 'Category updated successfully', 'category' => $category]);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()], 500);
        }
    }

    public function destroy(Category $category): JsonResponse
    {
        try {
            $category->delete();
            return response()->json(['success' => true, 'message' => 'Category deleted successfully']);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()], 500);
        }
    }
}
