<?php

namespace App\Http\Controllers\Admin;

use App\Constants\Constants;
use App\Constants\NamesForWebsiteSettings;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Page;
use App\Models\SetsOfWebsite;
use App\Services\AdminPanelService;
use Illuminate\Support\Facades\Log;

class AdminPanelController extends Controller
{
    protected AdminPanelService $adminPanelService;

    public function __construct(AdminPanelService $adminPanelService)
    {
        $this->middleware('auth');
        $this->middleware('check.auth.session');
        $this->adminPanelService = $adminPanelService;
    }

    public function showAdminPanel()
    {
        $title = $h1 = 'Панель администратора';

        $breadcrumbs = [
            ['title' => 'Admin Panel', 'route' => null]
        ];

        return view('admin.home', ['title' => $title, 'h1' => $h1, 'breadcrumbs' => $breadcrumbs]);
    }

    public function showAllSets()
    {
        $title = $h1 = 'Комплекты настроек для всех сайтов, с которых будет осуществляться парсинг';

        $setsOfWebsites = SetsOfWebsite::all();

        $breadcrumbs = [
            ['title' => 'Admin Panel', 'route' => route('admin')],
            ['title' => 'All Sets', 'route' => null]
        ];

        return view('admin.all_sets', [
            'setsOfWebsites' => $setsOfWebsites,
            'title' => $title,
            'h1' => $h1,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function showFormToCreateSet()
    {
        $title = $h1 = 'Добавить новый комплект настроек для парсинга';

        $rows = $this->adminPanelService->handleRows(Constants::EXCLUDED_KEYS_FOR_EDITING);

        $stringParameters = Constants::STRING_PARAMETERS;

        $categories = Category::all();

        $breadcrumbs = [
            ['title' => 'Admin Panel', 'route' => route('admin')],
            ['title' => 'All Sets', 'route' => route('admin.sets')],
            ['title' => 'Create Set', 'route' => null]
        ];

        return view('admin.create_set_of_website', [
            'title' => $title,
            'h1' => $h1,
            'rows' => $rows,
            'breadcrumbs' => $breadcrumbs,
            'categories' => $categories,
            'arrayOfStringParameters' => $stringParameters
        ]);
    }

    public function showSingleSet($id)
    {
        $set = SetsOfWebsite::with('pages.category')->findOrFail($id);
        $setArray = $set->toArray();

        $pagesWithCategories = collect($set->pages)->mapWithKeys(function ($page) {
            return [$page->url => $page->category ? $page->category->name : 'Без категории'];
        })->toArray();

        $setArrayWithPages = [];
        foreach ($setArray as $key => $value) {
            $setArrayWithPages[$key] = $value;

            if ($key === 'website_address') {
                $setArrayWithPages['pages'] = $pagesWithCategories;
            }
        }

        $title = $h1 = $set->name;

        $handledSetsArray = $this->adminPanelService->handleSetsArray($setArrayWithPages);

        $breadcrumbs = [
            ['title' => 'Admin Panel', 'route' => route('admin')],
            ['title' => 'All Sets', 'route' => route('admin.sets')],
            ['title' => $set->name, 'route' => null]
        ];

        return view('admin.single_set', [
            'setId' => $id,
            'title' => $title,
            'h1' => $h1,
            'handledSetsArray' => $handledSetsArray,
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    public function showFormToEditSet($id)
    {
        $set = SetsOfWebsite::with('pages.category')->findOrFail($id);
        $title = $h1 = $set->name;
        $rows = $this->adminPanelService->handleRows(Constants::EXCLUDED_KEYS_FOR_EDITING);

        $currentValues = $set->getAttributes();

        foreach ($currentValues as $key => $value) {
            if (is_string($value)) {
                $decodedValue = json_decode($value, true);
                if (json_last_error() === JSON_ERROR_NONE) {
                    $currentValues[$key] = $decodedValue;
                }
            }
        }

        $stringParameters = Constants::STRING_PARAMETERS;

        $categories = Category::all();

        $pages = $set->pages->map(function ($page) {
            return [
                'url' => $page->url,
                'found_urls_contain' => $page->found_urls_contain,
                'category_id' => $page->category ? $page->category->id : null,
                'category_name' => $page->category ? $page->category->name : 'Без категории'
            ];
        });

        $breadcrumbs = [
            ['title' => 'Admin Panel', 'route' => route('admin')],
            ['title' => 'All Sets', 'route' => route('admin.sets')],
            ['title' => $set->name, 'route' => route('admin.set.show', $set->id)],
            ['title' => 'Edit', 'route' => null]
        ];

        return view('admin.edit_set', [
            'setId' => $id,
            'title' => $title,
            'h1' => $h1,
            'rows' => $rows,
            'currentValues' => $currentValues,
            'categories' => $categories,
            'pages' => $pages,
            'breadcrumbs' => $breadcrumbs,
            'arrayOfStringParameters' => $stringParameters
        ]);
    }
}
