<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use App\Services\ArticlesService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ArticleController extends Controller
{
    protected $articlesService;

    public function __construct(ArticlesService $articlesService)
    {
        $this->articlesService = $articlesService;
    }

    public function index()
    {
        return view('articles.index');
    }

    public function allArticles()
    {
        $categories = Category::orderBy('name')->get();

        $articles = Article::orderBy('created_at', 'desc')->paginate(50);

        return view('articles.all_articles', compact('articles', 'categories'));
    }

    public function filterArticles(Request $request)
    {
        $categories = Category::orderBy('name')->get();

        if (!$request->query('date') && !$request->query('category')) {
            return redirect()->route('articles');
        }

        $query = Article::query();

        if ($date = $request->query('date')) {
            $date = Carbon::createFromFormat('d-m-Y', $date)->format('Y-m-d');
            $query->whereDate('created_at', $date);
        }

        $category = null;

        if ($categorySlug = $request->query('category')) {
            $category = Category::where('slug', $categorySlug)->first();
            if ($category) {
                $query->where('category_id', $category->id);
            }
        }

        $articles = $query->orderBy('created_at', 'desc')->paginate(50);

        return view('articles.all_articles', compact('articles', 'category', 'categories'));
    }

    public function show($slug)
    {
        $articleKey = "article_{$slug}";

        $article = Cache::rememberForever($articleKey, function () use ($slug) {
            return Article::where('slug', $slug)->first();
        });

        if (is_null($article)) {
            throw new NotFoundHttpException('Artile is not found');
        }

        $recommendedArticles = $this->articlesService->getRecommendedArticles($article->id);

        return view('articles.show', compact('article', 'recommendedArticles'));
    }

    public function allCategories()
    {
        $categories = Category::orderBy('name')->paginate(30);

        return view('articles.all_categories', compact('categories'));
    }

    public function category($categorySlug)
    {
        $category = Category::where('slug', $categorySlug)->firstOrFail();

        $articles = Article::where('category_id', $category->id)->orderBy('created_at', 'desc')->paginate(50);

        return view('articles.all_articles', compact('category', 'articles'));
    }

    public function handleSpecialPage(Request $request)
    {
        $pageType = $request->route('pageType');

        $specialPageTitle = $this->articlesService->getSpecialPageTitle($pageType);

        return view('special_pages.special_page', compact('pageType', 'specialPageTitle'));
    }
}
