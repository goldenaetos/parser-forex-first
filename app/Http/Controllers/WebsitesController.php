<?php

namespace App\Http\Controllers;

use App\Http\Requests\WebsiteCreateRequest;
use App\Http\Resources\WebsiteCreateResource;
use App\Models\Page;
use App\Models\SetsOfWebsite;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class WebsitesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('check.auth.session');
    }

    public function store(WebsiteCreateRequest $request): JsonResponse
    {
        try {
            $data = $request->validated();

            $setsOfWebsite = SetsOfWebsite::create($data);

            if (isset($data['pages'])) {
                foreach ($data['pages'] as $pageData) {
                    if (isset($pageData['url']) && isset($pageData['category_id'])) {
                        $url = $pageData['url'];
                        $categoryId = (int) $pageData['category_id'];
                        $foundUrlsContain = $pageData['found_urls_contain'] ?? null;

                        Page::updateOrCreate(
                            ['url' => $url, 'set_of_website_id' => $setsOfWebsite->id],
                            ['category_id' => $categoryId, 'found_urls_contain' => $foundUrlsContain]
                        );
                    }
                }
            }
            $websiteCreateResource = new WebsiteCreateResource($setsOfWebsite);

            return response()->json(['message' => 'Data added successfully', 'data' => $websiteCreateResource], Response::HTTP_CREATED);
        } catch (\Exception $exception) {
            Log::error('An error occurred while storing the sets of the website: ' . $exception->getMessage());

            return response()->json(['error' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function update(WebsiteCreateRequest $request, $id): JsonResponse
    {
        try {
            $setsOfWebsite = SetsOfWebsite::findOrFail($id);
            $data = $request->validated();

            $setsOfWebsite->update($data);

            $existingPageUrls = $setsOfWebsite->pages->pluck('url')->toArray();
            $newPageUrls = array_column($data['pages'], 'url');

            $pagesToDelete = array_diff($existingPageUrls, $newPageUrls);
            foreach ($pagesToDelete as $url) {
                Page::where('url', $url)
                    ->where('set_of_website_id', $setsOfWebsite->id)
                    ->delete();
            }

            foreach ($data['pages'] as $pageData) {
                if (isset($pageData['url']) && isset($pageData['category_id'])) {
                    $url = $pageData['url'];
                    $categoryId = (int) $pageData['category_id'];
                    $foundUrlsContain = $pageData['found_urls_contain'] ?? null;

                    Page::updateOrCreate(
                        ['url' => $url, 'set_of_website_id' => $setsOfWebsite->id],
                        ['category_id' => $categoryId, 'found_urls_contain' => $foundUrlsContain]
                    );
                }
            }

            return response()->json(['message' => 'Set updated successfully'], Response::HTTP_OK);
        } catch (\Exception $exception) {
            Log::error('An error occurred while updating the set of the website: ' . $exception->getMessage());

            return response()->json(['error' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function destroy($id)
    {
        try {
            $setsOfWebsite = SetsOfWebsite::findOrFail($id);

            $setsOfWebsite->delete();

            return response()->json(['message' => 'The set is successfully deleted'], Response::HTTP_OK);
        } catch (\Exception $exception) {
            Log::error('An error occurred while deleting the set of the website: ' . $exception->getMessage());

            return response()->json(['error' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
