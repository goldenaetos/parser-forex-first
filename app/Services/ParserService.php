<?php

namespace App\Services;

use App\Constants\Constants;
use App\Models\Article;
use App\Models\SetsOfWebsite;
use App\Models\UsedUrl;
use Carbon\Carbon;
use Exception;
use Google\Cloud\Core\Exception\GoogleException;
use Google\Cloud\Core\Exception\ServiceException;
use Goutte\Client;
use Illuminate\Http\Response;
use Google\Cloud\Translate\V2\TranslateClient;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ParserService
{
    public function handleEmptyUrls($currentMethodId, $methodIds)
    {
        $methodIds = array_diff($methodIds, [$currentMethodId]);
        if (!$methodIds) {
            $methodIds = 'Empty';
        }
        return $methodIds;
    }

    public function detectUniquePages($id, $url, $sectionOfWebsite, $stringStart, $stringEnd, $pattern)
    {
        $pageContentResult = $this->fetchPageContent($url);

        if (!$pageContentResult['success']) {
            return response()->json($pageContentResult['error'], Response::HTTP_NO_CONTENT);
        }

        $jsonContentResult = $this->extractJsonContent($pageContentResult['content'], $stringStart, $stringEnd, $url);

        if (!$jsonContentResult['success']) {
            return response()->json($jsonContentResult['error'], Response::HTTP_NO_CONTENT);
        }

        $singlePages = $this->findSinglePagesInJson($jsonContentResult['content'], $pattern, $id, $sectionOfWebsite);

        $existingPages = UsedUrl::where('url', $url)->pluck('page')->toArray();

        return array_diff($singlePages, $existingPages);
    }

    private function fetchPageContent($url): array
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3');
        $pageContent = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($httpcode != 200) {
            return ['success' => false, 'error' => ['error_message' => 'Failed to fetch page content from url: ' . $url . ' with HTTP code ' . $httpcode]];
        }

        return ['success' => true, 'content' => $pageContent];
    }

    private function extractJsonContent($pageContent, $stringStart, $stringEnd, $url): array
    {
        $jsonStart = strpos($pageContent, $stringStart);

        if ($jsonStart === false) {
            return ['success' => false, 'error' => ['error_message' => 'Failed to detect jsonStart from url: ' . $url]];
        }

        $jsonEnd = strpos($pageContent, $stringEnd, $jsonStart);
        if ($jsonEnd === false) {
            return ['success' => false, 'error' => ['error_message' => 'Failed to detect jsonEnd from url: ' . $url]];
        }

        $jsonContent = substr($pageContent, $jsonStart, $jsonEnd - $jsonStart);

        return ['success' => true, 'content' => $jsonContent];
    }

    private function findSinglePagesInJson($jsonContent, $pattern, $id, $sectionOfWebsite): array
    {
        preg_match_all($pattern, $jsonContent, $matches);
        $singlePages = $matches[1];

        $setOfWebsite = SetsOfWebsite::find($id);
        if (!$setOfWebsite) {
            return [];
        }

        $foundUrlsContain = $sectionOfWebsite->found_urls_contain;
        $inappropriatePages = $this->detectInappropriatePages($setOfWebsite);
        $singlePages = array_unique($singlePages);
        $singlePages = $this->addWebsiteAddress($setOfWebsite, $singlePages);

        $filteredPages = [];
        foreach ($singlePages as $page) {
            $isAppropriate = true;

            foreach ($inappropriatePages as $inappPage) {
                if (strpos($page, $inappPage) !== false) {
                    $isAppropriate = false;
                    break;
                }
            }

            if ($isAppropriate && ($foundUrlsContain === null || strpos($page, $foundUrlsContain) !== false)) {
                $filteredPages[] = $page;
            }
        }

        return array_filter($filteredPages, function ($page) {
            $parsedUrl = parse_url($page);
            $path = $parsedUrl['path'] ?? '';
            return strpos($path, '-') !== false;
        });
    }

    private function addWebsiteAddress($setOfWebsite, $singlePages)
    {
        $websiteAddress = rtrim($setOfWebsite->website_address, '/');

        foreach ($singlePages as &$page) {
            if (strpos($page, '/') === 0) {
                $page = $websiteAddress . $page;
            }
        }
        unset($page);

        return $singlePages;
    }

    private function detectInappropriatePages($setOfWebsite)
    {
        $inappropriatePages = $setOfWebsite->inappropriate_pages;

        if(empty($inappropriatePages)) {
            return [];
        }

        return json_decode($inappropriatePages, true);
    }

    public function detectSinglePage($uniquePages)
    {
        $uniquePages = array_values($uniquePages);

        return $uniquePages[array_rand($uniquePages)];
    }

    public function saveUrlOfSinglePage($url, $singlePage)
    {
        UsedUrl::create([
            'url' => $url,
            'page' => $singlePage
        ]);
    }

    public function handleEmptyUniquePages($url, $urls)
    {
        $urls = array_diff($urls, [$url]);
        if (count($urls) < 1) {
            $urls = 'empty';
        }
        return $urls;
    }

    public function parseArticlesTitle($url)
    {
        $client = new Client();
        $crawler = $client->request('GET', $url);
        $title = $crawler->filter('title')->text();

        if (!$title) {
            return response()->json(['error_message' => 'There are no tags title in parsed content from ' . $url], Response::HTTP_NO_CONTENT);
        }

        $title = preg_replace('/ \|.*$/', '', $title);

        return $this->handlerForCharset($title);
    }

    private function handlerForCharset($content): string
    {
        $content = preg_replace('/’’/', '’', $content);
        $content = preg_replace('/\\\u00e2\\\u0080\\\u0098/', "‘", $content);
        $content = preg_replace('/\\\u00e2\\\u0080\\\u0099/', "’", $content);
        $content = preg_replace('/(\\\u00e2\\\u0080\\\u009c|\\\u2018)/', "‘", $content);
        $content = preg_replace('/(\\\u00e2\\\u0080\\\u009d|\\\u2019)/', "’", $content);
        $content = preg_replace('/\\\u201c/', "“", $content);
        $content = preg_replace('/\\\u201d/', "”", $content);
        $content = preg_replace('/\\\u00ab/', "«", $content);
        $content = preg_replace('/\\\u00bb/', "»", $content);
        $content = preg_replace('/\\\u2019/', "’", $content);
        $content = preg_replace('/,’/', "’,", $content);
        $content = preg_replace('/\.’/', "’.", $content);
        $content = preg_replace('/,”/', "”,", $content);
        $content = preg_replace('/\.”/', "”.", $content);
        $content = preg_replace('/\\\u00c2\\\u00a0/', " ", $content);
        $content = str_replace('&nbsp;', ' ', $content);
        $content = preg_replace('/(\\\u00e2\\\u0080\\\u0093|\\\u00e2\\\u0080\\\u0094|\\\u2013|\\\u2014|—)/', " – ", $content);
        $content = preg_replace('/\\\u00e2\\\u0080\\\u00a6/', "…", $content);
        $content = preg_replace('/\s-\s/', " – ", $content);
        $content = preg_replace('/(\s+|\\\u00a0)/', " ", $content);
        $content = preg_replace('/\\\u00c3\\\u00a0/', "á", $content);
        $content = preg_replace('/\\\u00c3\\\u00a1/', "à", $content);
        $content = preg_replace('/\\\u00c3\\\u00a9/', "é", $content);
        $content = preg_replace('/\\\u00c3\\\u00ab/', 'ë', $content);
        $content = preg_replace('/\\\u00c3\\\u00af/', "ï", $content);
        $content = preg_replace('/\\\u00c3\\\u00ba/', 'ú', $content);
        $content = preg_replace('/\\\u00c3\\\u00a3/', 'ã', $content);
        $content = preg_replace('/\\\u00c3\\\u00bc/', 'ü', $content);
        $content = preg_replace('/\\\u00c3\\\u00ad/', 'í', $content);
        $content = preg_replace('/\\\u00c3\\\u00b3/', 'ó', $content);
        $content = preg_replace('/\\\u00c3\\\u00b1/', 'ñ', $content);
        $content = preg_replace('/\\\u00c2(?=\\\u00a3)/', '', $content);
        $content = preg_replace('/\\\u00e2\\\u0080\\\u008b/', '', $content);
        $content = preg_replace('/\\\u00c2\\\u00ad/', '', $content);
        $content = preg_replace('/\\\u00c2\\\u00bd/', '½', $content);
        $content = preg_replace('/\\\u00c2\\\u00bc/', '¼', $content);
        $content = preg_replace('/\\\u00c2\\\u00be/', '¾', $content);
        $content = preg_replace('/\\\u00c2\\\u215b/', '⅛', $content);
        $content = preg_replace('/\\\u00c2\\\u215c/', '⅜', $content);
        $content = preg_replace('/\\\u00c2\\\u215d/', '⅝', $content);
        $content = preg_replace('/\\\u00c2\\\u215e/', '⅞', $content);
        $content = preg_replace('/\\\u00e2\\\u0082\\\u00ac/', '€', $content);
        $content = preg_replace('/\\\u00c2\\\u00a3/', '£', $content);
        $content = preg_replace('/\\\u00e2\\\u0082\\\u00a9/', '¥', $content);
        $content = preg_replace('/\\\u00c2\\\u0024/', '$', $content);
        $content = preg_replace('/\\\u00c2\\\u00a9/', '©', $content);

        return trim($content);
    }

    private function buildExcludeClassesXPath($classNames, $tagName): string
    {
        $conditions = array_map(function($className) {
            return "not(contains(@class, '$className'))";
        }, $classNames);

        if (empty($conditions)) {
            return "//{$tagName}";
        }

        $excludeCondition = implode(' and ', $conditions);

        return "//{$tagName}[{$excludeCondition}]";
    }

    public function parseArticlesContent($url, $id, $title, $imageTail): array
    {
        $client = new Client();
        $crawler = $client->request('GET', $url);

        $dom = new \DOMDocument();
        @$dom->loadHTML($crawler->html());
        $xpath = new \DOMXPath($dom);

        $content = [];
        $setsOfWebsite = SetsOfWebsite::findOrFail($id);
        $stopWords = json_decode($setsOfWebsite->stop_words);

        $xpathQueryParts = $this->createXPathQuery($setsOfWebsite);

        $nodes = $xpath->query(implode(" | ", $xpathQueryParts));

        foreach ($nodes as $node) {

            $foundStopWord = false;

            foreach ($stopWords as $stopWord) {
                if (strpos($node->textContent, $stopWord) !== false) {
                    $foundStopWord = true;
                    break;
                }
            }

            if ($foundStopWord) {
                break;
            }

            if ($node->nodeName === 'p') {
                $content[] = ['type' => 'text', 'content'  => trim($node->textContent), 'class' => $node->getAttribute('class')];
            } elseif (in_array($node->nodeName, ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'])) {
                $content[] = ['type' => 'heading', 'level' => intval(substr($node->nodeName, 1)), 'content' => trim($node->textContent), 'class' => $node->getAttribute('class')];
            } elseif ($node->nodeName === 'img' && $node->parentNode->nodeName !== 'picture') {
                $content[] = ['type' => 'image', 'content' => $node->getAttribute('src'), 'class' => $node->getAttribute('class')];
            } elseif ($node->nodeName === 'picture') {
                $sourceNodes = $xpath->query('source', $node);
                $imgNode = $xpath->query('img', $node)->item(0);

                foreach ($sourceNodes as $source) {
                    $srcset = $source->getAttribute('srcset');
                    $sources = explode(', ', $srcset);
                    $content[] = ['type' => 'image', 'content' => trim(explode(' ', $sources[0])[0])];
                }

                if ($imgNode) {
                    $content[] = ['type' => 'image', 'content' => $imgNode->getAttribute('src')];
                }
            } elseif ($node->nodeName === 'figure') {
                $imgNodes = $xpath->query('.//progressive-image', $node);
                if ($imgNodes->length > 0) {
                    foreach ($imgNodes as $imgNode) {
                        $imgSrc = $imgNode->getAttribute('src');
                        if ($imgSrc) {
                            $content[] = ['type' => 'image', 'content' => $imgSrc];
                        }
                    }
                }
            }
        }

        $content = $this->ensureH1TagExists($content, $title);

        return $this->filterArticlesContent($content, $id, $imageTail);
    }

    private function createXPathQuery($setsOfWebsite): array
    {
        $tags = ['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'img'];

        if (!empty($setsOfWebsite->inappropriate_tag)) {
            $tags = array_diff($tags, [$setsOfWebsite->inappropriate_tag]);
        }

        $queryParts = [];
        foreach ($tags as $tagName) {
            $excludeClassVar = "exclude_class_for_{$tagName}";
            $excludeClasses = json_decode($setsOfWebsite->$excludeClassVar ?? '[]', true);
            $queryParts[] = $this->buildExcludeClassesXPath($excludeClasses, $tagName);
        }

        $queryParts[] = "//picture";
        $queryParts[] = "//figure";

        return $queryParts;
    }

    private function ensureH1TagExists($content, $title)
    {
        $h1Exists = false;
        foreach ($content as $item) {
            if ($item['type'] === 'heading' && $item['level'] === 1) {
                $h1Exists = true;
                break;
            }
        }

        if (!$h1Exists) {
            array_unshift($content, ['type' => 'heading', 'level' => 1, 'content' => $title]);
        }

        return $content;
    }

    private function filterArticlesContent(array $content, $id, $imageTail): array
    {
        $setsOfWebsite = SetsOfWebsite::findOrFail($id);
        $textEquals = json_decode($setsOfWebsite->text_equals);
        $textStartsWith = json_decode($setsOfWebsite->text_starts_with);
        $textStartsWithPatterns = json_decode($setsOfWebsite->text_starts_with_pattern);
        $textContains = json_decode($setsOfWebsite->text_contains);
        $pathToImageContains = json_decode($setsOfWebsite->path_to_image_contains);
        $excludeClassForImg = json_decode($setsOfWebsite->exclude_class_for_img);
        $excludeClassForH = json_decode($setsOfWebsite->exclude_class_for_h);

        $filteredContent = array_filter($content, function ($item) use ($textEquals, $textStartsWith, $textStartsWithPatterns, $textContains, $pathToImageContains, $excludeClassForImg, $excludeClassForH) {
            foreach ($textEquals as $textString) {
                if (strpos($item['content'], $textString) !== false) {
                    return false;
                }
            }

            foreach ($textStartsWith as $startString) {
                if (strpos($item['content'], $startString) === 0) {
                    return false;
                }
            }

            foreach ($textStartsWithPatterns as $pattern) {
                if (preg_match($pattern, $item['content']) && strlen($item['content']) < 31 && strpos($item['content'], '.') === false) {
                    return false;
                }
            }

            foreach ($textContains as $text) {
                if (strpos($item['content'], $text) !== false) {
                    return false;
                }
            }

            foreach ($pathToImageContains as $partOfPath) {
                if ($item['type'] === 'image' && strpos($item['content'], $partOfPath) !== false) {
                    return false;
                }
            }

            if ($item['type'] === 'image' && isset($item['class']) && !empty($excludeClassForImg)) {
                foreach ($excludeClassForImg as $class) {
                    if (strpos($item['class'], $class) !== false) {
                        return false;
                    }
                }
            }

            if ($item['type'] === 'heading' && isset($item['class']) && !empty($excludeClassForH)) {
                foreach ($excludeClassForH as $class) {
                    if (strpos($item['class'], $class) !== false) {
                        return false;
                    }
                }
            }

            return true;
        });

        $filteredContent = $this->excludeEmptyContentImages($filteredContent);
        $filteredContent = $this->removeImageParams($filteredContent, $id);

        if ($imageTail) {
            $filteredContent = $this->removeNonuniqueImages($filteredContent);
        }

        return array_values($filteredContent);
    }

    private function excludeEmptyContentImages($content): array
    {
        return array_filter($content, function ($item) {
            if ($item['type'] === 'image') {
                return !empty($item['content']);
            }
            return true;
        });
    }

    private function removeImageParams(array &$content, $id): array
    {
        $setsOfWebsite = SetsOfWebsite::findOrFail($id);
        $removeImageParamsPatterns = json_decode($setsOfWebsite->remove_image_params_patterns);

        foreach ($removeImageParamsPatterns as $patternForRemove) {
            $occurrenceFirst = $patternForRemove->occurrence;
            $occurrenceLast = $patternForRemove->occurrence_additional;
            $pattern = $patternForRemove->pattern;

            if (isset($occurrenceLast)) {
                foreach ($content as &$item) {
                    if ($item['type'] === 'image' && (strpos($item['content'], $occurrenceFirst) && strpos($item['content'], $occurrenceLast)) ) {
                        $item['content'] = preg_replace($pattern, '', $item['content']);
                    }
                }
            }

            if (!isset($occurrenceLast)) {
                foreach ($content as &$item) {
                    if ($item['type'] === 'image' && (strpos($item['content'], $occurrenceFirst))) {
                        $item['content'] = preg_replace($pattern, '', $item['content']);
                    }
                }
            }

            unset($item);
        }

        return $content;
    }

    /**
     * @throws GoogleException
     * @throws ServiceException
     * @throws Exception
     */
    private function translateContent(array $content, string $targetLanguage = Constants::TARGET_LANGUAGE): array
    {
        $totalLength = 0;
        foreach ($content as $item) {
            if (isset($item['type']) && ($item['type'] === 'text' || $item['type'] === 'heading')) {
                $totalLength += mb_strlen($item['content']);
            }
        }

        if ($totalLength > 50000) {
            throw new Exception('The text exceeds the maximum size limit for translation');
        }

        $translate = new TranslateClient([
            'key' => config('services.google_translate.key'),
        ]);

        $translatedContent = [];
        foreach ($content as $item) {
            if (isset($item['type']) && ($item['type'] === 'text' || $item['type'] === 'heading')) {
                $translation = $translate->translate($item['content'], ['target' => $targetLanguage]);
                $decodedContent = html_entity_decode($translation['text'], ENT_QUOTES, 'UTF-8');
                $item['content'] = str_replace("'", "’", $decodedContent);
            }
            $translatedContent[] = $item;
        }

        return $translatedContent;
    }

    public function saveArticle($title, $content, $categoryId = 0, $imageTail)
    {
        $targetLanguage = Constants::TARGET_LANGUAGE;

        $translatedTitle = $this->translateContent([['type' => 'heading', 'content' => $title]], $targetLanguage)[0]['content'];
        $translatedContent = $this->translateContent($content, $targetLanguage);

        $dateNow = Carbon::now()->format('Y-m-d');
        $slug = Str::slug($translatedTitle) . '-' . $dateNow;

        $article = new Article();
        $article->title = $translatedTitle;
        $article->category_id = $categoryId;
        $article->slug = $slug;

        $previewProcessed = false;

        foreach ($translatedContent as $key => &$item) {
            if ($item['type'] === 'image') {
                $imagePath = strtok($item['content'], '?');
                if ($imagePath) {
                    if ($imageTail) {
                        $imagePath .= $imageTail;
                    }
                    $fileName = $this->processArticleImage($imagePath);
                    if ($fileName) {
                        $newImagePath = asset('images/' . $fileName);
                        $item['content'] = $newImagePath;

                        if (!$previewProcessed) {
                            $this->processImageForPreview($imagePath, $article, $fileName);
                            $previewProcessed = true;
                        }
                    } else {
                        unset($translatedContent[$key]);
                    }
                }
            }
        }

        $contentJson = json_encode($translatedContent);
        $article->content = $this->handlerForCharset($contentJson);
        $article->save();
    }

    private function processArticleImage($imagePath)
    {
        $image = Image::make($imagePath)->orientate();

        if ($image->width() == 1 && $image->height() == 1) {
            return null;
        }

        $maxWidth = 1600;
        $maxFileSize = 200 * 1024;

        $processedImage = $this->resizeAndCompressImage($image, $maxWidth, $maxFileSize);
        $fileName = Str::random(20) . '.webp';
        $localImagePath = 'images/' . $fileName;
        File::put(public_path($localImagePath), (string) $processedImage);

        return $fileName;
    }

    private function processImageForPreview($imagePath, $article, $fileName)
    {
        $previewMaxFileSize = 25 * 1024;
        $previewImage = $this->createSquarePreview($imagePath, 200, $previewMaxFileSize);
        $smallPreviewImage = $this->createSquarePreview($imagePath, 100, $previewMaxFileSize);
        $previewImagePath = 'images/previews/' . $fileName;
        $smallPreviewImagePath = 'images/small_previews/' . $fileName;
        File::put(public_path($previewImagePath), (string) $previewImage);
        File::put(public_path($smallPreviewImagePath), (string) $smallPreviewImage);
        $article->preview = $previewImagePath;
        $article->small_preview = $smallPreviewImagePath;
    }

    private function createSquarePreview($imagePath, $size, $maxFileSize) {
        $image = Image::make($imagePath)->orientate();
        $image->fit($size, $size, function ($constraint) {
            $constraint->upsize();
        });

        return $this->compressImage($image, $maxFileSize);
    }

    private function resizeAndCompressImage($image, $maxWidth, $maxFileSize)
    {
        if ($image->width() > $maxWidth) {
            $image->resize($maxWidth, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
        }

        return $this->compressImage($image, $maxFileSize);
    }

    private function compressImage($image, $maxFileSize)
    {
        $quality = 90;
        $step = 10;
        $encodedImage = $image->encode('webp', $quality);
        while ($quality > 0 && strlen((string) $encodedImage) > $maxFileSize) {
            $quality -= $step;
            $encodedImage = $image->encode('webp', $quality);
            if ($quality <= 10) {
                $step = 1;
            }
        }

        return $encodedImage;
    }

    private function removeNonuniqueImages($filteredContent)
    {
        $result = [];
        $lastImageBase = null;
        $currentImageGroup = [];

        foreach ($filteredContent as $item) {
            if ($item['type'] === 'image') {
                $baseContent = explode('?', $item['content'])[0];

                if ($baseContent === $lastImageBase) {
                    $currentImageGroup[] = $item;
                } else {
                    if (count($currentImageGroup) > 0) {
                        $result[] = ['type' => 'image', 'content' => explode('?', $currentImageGroup[0]['content'])[0]];
                    }
                    $currentImageGroup = [$item];
                    $lastImageBase = $baseContent;
                }
            } else {
                if (count($currentImageGroup) > 0) {
                    $result[] = ['type' => 'image', 'content' => explode('?', $currentImageGroup[0]['content'])[0]];
                    $currentImageGroup = [];
                    $lastImageBase = null;
                }
                $result[] = $item;
            }
        }

        if (count($currentImageGroup) > 0) {
            $result[] = ['type' => 'image', 'content' => explode('?', $currentImageGroup[0]['content'])[0]];
        }

        return $result;
    }
}
