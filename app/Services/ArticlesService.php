<?php

namespace App\Services;

use App\Constants\NavigationConstants;
use App\Models\Article;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class ArticlesService
{
    public function getRecommendedArticles($excludeId = null)
    {
        $today = Carbon::now();
        $startOfLastMonth = $today->copy()->subMonthNoOverflow()->startOfMonth();
        $endOfLastMonth = $startOfLastMonth->copy()->endOfMonth();
        $startOfCurrentMonth = $today->copy()->startOfMonth();
        $endOfCurrentMonth = $today->copy()->endOfMonth();

        $recommendedArticles = collect();

        if ($today->day <= 7) {
            $recommendedArticles = Article::whereBetween('created_at', [$startOfLastMonth, $endOfLastMonth])
                ->where('id', '!=', $excludeId)
                ->inRandomOrder()
                ->take(2)
                ->get();
        }

        if ($today->day >= 8 || $recommendedArticles->count() < 2) {
            $additionalArticlesNeeded = 2 - $recommendedArticles->count();
            $additionalArticles = Article::whereBetween('created_at', [$startOfCurrentMonth, $endOfCurrentMonth])
                ->where('id', '!=', $excludeId)
                ->whereNotIn('id', $recommendedArticles->pluck('id'))
                ->inRandomOrder()
                ->take($additionalArticlesNeeded)
                ->get();

            $recommendedArticles = $recommendedArticles->merge($additionalArticles);
        }

        return $recommendedArticles;
    }

    public function getUniqueCategoryArticles(): Collection
    {
        $latestArticles = Article::latest()->get();
        $uniqueCategoryArticles = collect();
        $addedCategoryIds = [];

        foreach ($latestArticles as $article) {
            if (!in_array($article->category_id, $addedCategoryIds)) {
                $uniqueCategoryArticles->push($article);
                $addedCategoryIds[] = $article->category_id;
            }

            if ($uniqueCategoryArticles->count() >= 4) {
                break;
            }
        }

        return $uniqueCategoryArticles;
    }

    public function getSpecialPageTitle($pageType): string
    {
        $titles = [
            'recommendations' => NavigationConstants::RECOMMENDATIONS,
            'partners' => NavigationConstants::PARTNERS,
            'offer' => NavigationConstants::OFFER
        ];

        return $titles[$pageType];
    }
}
