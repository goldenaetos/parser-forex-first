<?php

namespace App\Services;

use App\Constants\Constants;
use App\Constants\NamesForWebsiteSettingsParameters;
use Carbon\Carbon;

class AdminPanelService
{
    protected array $handledSetsArray = [];
    protected int $counter = 1;

    public function handleSetsArray($setArrayWithPages, $editing = false): array
    {
        $customLabels = NamesForWebsiteSettingsParameters::getAllConstantsToLowerCaseWithValues();

        foreach ($setArrayWithPages as $key => $value) {
            if (in_array($key, Constants::EXCLUDED_KEYS_FOR_SHOWING)) {
                continue;
            }

            if ($editing && (in_array($key, Constants::EXCLUDED_KEYS_FOR_EDITING))) {
                continue;
            }

            $label = $customLabels[$key] ?? $key;

            if ($key === 'created_at' || $key === 'updated_at') {
                $value = Carbon::parse($value)->format('d.m.Y в H:i:s');
            }

            if (is_string($value) && is_array(json_decode($value, true)) && (json_last_error() == JSON_ERROR_NONE)) {
                $value = json_decode($value, true);
            }

            $formattedCounter = sprintf('%02d', $this->counter);
            $this->handledSetsArray[$formattedCounter . $label] = $value;

            $this->counter++;
        }

        return $this->handledSetsArray;
    }

    public function handleRows($excludedKeys): array
    {
        $rows = array_diff_key(NamesForWebsiteSettingsParameters::getAllConstantsToLowerCaseWithValues(), array_flip($excludedKeys));

        foreach ($rows as $key => $value) {
            $formattedCounter = sprintf('%02d', $this->counter);

            $this->handledSetsArray[$key] = $formattedCounter . $value;

            $this->counter++;
        }

        return $this->handledSetsArray;
    }
}
