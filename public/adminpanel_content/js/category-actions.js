function deleteCategory(button) {
    const url = button.getAttribute('data-route');

    fetch(url, {
        method: 'DELETE',
        headers: {
            'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok ' + response.statusText);
            }
            return response.json();
        })
        .then(data => {
            console.log('Success:', data);
            removeCategoryRow(button);
        })
        .catch((error) => {
            console.error('Error:', error);
        });
}

function removeCategoryRow(button) {
    let row = button.closest('tr');
    if (row) {
        row.remove();
    } else {
        window.location.href = '/admin/categories';
    }
}

document.addEventListener('DOMContentLoaded', function () {
    const deleteCategoryButtons = document.querySelectorAll('.delete-set');

    deleteCategoryButtons.forEach(button => {
        button.addEventListener('click', function (event) {
            event.preventDefault();
            if (confirm('Вы уверены, что хотите удалить эту категорию?')) {
                deleteCategory(this);
            }
        });
    });
});

function createEditRow(button, categoryName, parentElement) {
    if (parentElement.nextElementSibling?.classList.contains('edit-row')) {
        return;
    }

    let editRow = document.createElement('tr');
    editRow.className = 'edit-row';
    editRow.innerHTML = `
        <td colspan="4">
            <input type="text" class="form-control" value="${categoryName}" />
            <button class="btn btn-primary save-edit">Сохранить</button>
            <button class="btn btn-secondary cancel-edit">Отмена</button>
        </td>
    `;
    parentElement.after(editRow);

    editRow.querySelector('.save-edit').addEventListener('click', function() {
        let newValue = editRow.querySelector('input').value.trim();
        if (!newValue) {
            console.warn('Название категории не может быть пустым');
            return;
        }
        if (newValue === categoryName) {
            editRow.remove();
            return;
        }

        let categoryId = button.getAttribute('data-category-id');

        fetch(`/admin/categories/${categoryId}`, {
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ name: newValue, _method: 'PUT' })
        })
            .then(response => {
                if (!response.ok) {
                    console.log(response);
                    throw new Error('Network response was not ok');
                }
                return response.json();
            })
            .then(data => {
                console.log('Success:', data);
                if (parentElement.tagName === 'TR') {
                    let linkElement = parentElement.querySelector('td:first-child a');
                    if (linkElement) {
                        linkElement.textContent = newValue;
                    } else {
                        parentElement.querySelector('td:first-child').textContent = newValue;
                    }
                } else {
                    document.querySelector('.category-name').textContent = newValue;
                    document.querySelector('h1').textContent = newValue;
                    document.querySelector('title').textContent = newValue;
                }
                editRow.remove();
            })
            .catch(error => {
                console.error('Error:', error);
            });
    });

    editRow.querySelector('.cancel-edit').addEventListener('click', function() {
        editRow.remove();
    });
}

document.addEventListener('DOMContentLoaded', function () {
    const editCategoryButtons = document.querySelectorAll('.edit-set');

    editCategoryButtons.forEach(button => {
        button.addEventListener('click', function (event) {
            event.preventDefault();
            let parentElement;
            let currentCategoryName;

            if (this.closest('tr')) {
                parentElement = this.closest('tr');
                currentCategoryName = parentElement.querySelector('td:first-child').textContent.trim();
            } else {
                parentElement = this.closest('div.row');
                currentCategoryName = document.querySelector('.category-name').textContent.trim();
            }

            createEditRow(this, currentCategoryName, parentElement);
        });
    });
});
