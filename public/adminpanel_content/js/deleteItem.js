function deleteItem(button, urlForRedirect = null) {
    const path = button.getAttribute('data-route');

    fetch(path, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            "X-CSRF-TOKEN": document.querySelector('meta[name="csrf-token"]').getAttribute('content')
        }
    })
        .then(response => {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error('Error while deleting the set');
            }
        })
        .then(data => {
            console.log(data.message);
            const removingRow = button.closest('tr.row-of-sets');
            if (removingRow) {
                removingRow.remove();
            }
            if (urlForRedirect) {
                window.location.href = urlForRedirect;
            }
        })
        .catch(error => {
            console.log(error);
        });
}
