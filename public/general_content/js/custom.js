document.addEventListener('DOMContentLoaded', (event) => {
    const currentDataElement = document.getElementById('current-date');
    const options = { weekday: 'long', year: 'numeric', month: 'short', day: 'numeric', timeZone: 'Europe/Kiev' };
    currentDataElement.textContent = "\u00A0\u00A0" + new Intl.DateTimeFormat('en-US', options).format(new Date());

    $('.link-block').click(function() {
        const url = $(this).data('href');
        window.open(url, '_blank');
    });

    $('.banner-2').mouseenter(function(){
        $(this).find('.disappearing-text').fadeOut();
        $(this).find('.btn-primary').css({
            'border-color': '#0056b3',
            'background-color': '#0056b3'
        });
    });

    $('.banner-2').mouseleave(function(){
        $(this).find('.disappearing-text').fadeIn();
        $(this).find('.btn-primary').css({
            'border-color': '#1b7dff',
            'background-color': '#1b7dff'
        });
    });
});

$(window).on('load', function () {
    bannerOriginalPosition = $('.view-more').offset().top + $('.view-more').outerHeight() + 65;
    adjustBannerPosition();
});
$(window).on('scroll', adjustBannerPosition);

function adjustBannerPosition() {
    const $banner = $('.banner-2').parent();
    const $footer = $('.footer');
    const bannerHeight = $banner.outerHeight();
    const footerHeight = $footer.outerHeight();
    const documentHeight = $(document).height();
    const requiredHeight = bannerOriginalPosition + bannerHeight + footerHeight + 40;

    if (requiredHeight >= documentHeight) {
        return;
    }

    if ($(window).width() >= 992) {
        const bannerWidth = $banner.outerWidth();
        const scrollTop = $(window).scrollTop();
        const windowHeight = $(window).height();
        const footerOffset = $footer.offset().top;
        const remInPixels = parseFloat(getComputedStyle(document.documentElement).fontSize);
        const distanceFromWindowBottomToFooter = footerOffset - scrollTop - windowHeight;

        if (distanceFromWindowBottomToFooter <= (0.5 * remInPixels)) {
            $banner.css({
                position: 'fixed',
                width: bannerWidth,
                bottom: windowHeight - (footerOffset - scrollTop) + 40
            });
        } else if (scrollTop + windowHeight >= bannerOriginalPosition + bannerHeight + 0.5 * remInPixels) {
            $banner.css({
                position: 'fixed',
                width: bannerWidth,
                bottom: '50px'
            });
        } else if (scrollTop <= bannerOriginalPosition) {
            $banner.css({
                position: 'static',
                width: 'auto'
            });
        }
    }
}
